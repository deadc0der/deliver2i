Projet Deliver2I

En raison de la limite de taille des fichiers envoyables sur Moodle, nous 
n'avons pas pu envoyer 2 éléments importants du projet:
 - Certains des fichiers d'instance (dossier /Data) ne sont pas inclus.
 - Les librairies CPLEX nécessaires à notre algorithme (dossier /CPLEX).

Ces fichiers sont disponibles sur notre dépot Git disponible à l'adresse:
https://gitlab.com/deadc0der/deliver2i

On note également que les librairies CPLEX présentes sur notre dépôt Git 
n'incluent que les binaires pour Mac. Pour utiliser les binaires d'une autre
plateforme, veuillez les placer dans le dossier /CPLEX.

Merci de votre compréhension.
