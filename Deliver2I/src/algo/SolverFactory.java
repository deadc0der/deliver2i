package algo;

/**
 * Factory class used to create solvers. This is most notably used by the Lab to
 * allow the user to select and run one.
 */
public class SolverFactory {

	/**
	 * Types of Generators (GenerativeSolver).
	 */
	public enum Generator {
		Naive,
		Time,
		Random,
		RandomV2,
		CPLEX,
		Meta,
	}

	/**
	 * Types of Optimizers (OptimizingSolver).
	 */
	public enum Optimizer {
		Nope,
		RoundMerging,
		RoundMergingRandom,
		ChangeLocation,
	}

	/**
	 * Creates an instance of the requested generator.
	 *
	 * @param kind The requested kind of generator.
	 * @return The created generator.
	 * @throws algo.SolverFactory.GeneratorNotFound If the requested generator
	 * can't be found.
	 */
	public static GenerativeSolver createGenerator(Generator kind) throws GeneratorNotFound {
		switch (kind) {
			case Naive:
				return new NaiveSolver();
			case Time:
				return new TimeSolver();
			case Random:
				return new RandomSolver();
			case RandomV2:
				return new RandomSolverV2();
			case CPLEX:
				return new CPLEXSolver();
			case Meta:
				return new MetaSolver();
			default:
				throw new GeneratorNotFound(kind);
		}
	}

	/**
	 * Creates an instance of the requested optimizer.
	 *
	 * @param kind The requested kind of optimizer.
	 * @return The created optimizer.
	 * @throws algo.SolverFactory.OptimizerNotFound If the requested optimizer
	 * can't be found.
	 */
	public static OptimizingSolver createOptimizer(Optimizer kind) throws OptimizerNotFound {
		switch (kind) {
			case Nope:
				return new NopeOptimizer();
			case RoundMerging:
				return new RoundMergingOptimizer();
			case RoundMergingRandom:
				return new RoundMergingRandom();
			case ChangeLocation:
				return new ChangeLocationOrderOptimizer();
			default:
				throw new OptimizerNotFound(kind);
		}
	}

	/**
	 * Exception thrown when the factory is asked to produce a generator it
	 * doesn't know about.
	 */
	public static class GeneratorNotFound extends Exception {

		private Generator kind;

		public GeneratorNotFound(Generator kind) {
			super(String.format("Generator not found: %s", kind));
			this.kind = kind;
		}

		public Generator getKind() {
			return kind;
		}
	}

	/**
	 * Exception thrown when the factory is asked to produce an optimizer it
	 * doesn't know about.
	 */
	public static class OptimizerNotFound extends Exception {

		private Optimizer kind;

		public OptimizerNotFound(Optimizer kind) {
			super(String.format("Optimizer not found: %s", kind));
			this.kind = kind;
		}

		public Optimizer getKind() {
			return kind;
		}
	}
}
