package algo;

/**
 * Error class thrown when a solver doesn't find any solution.
 */
public class NoSolutionError extends Exception {

	public NoSolutionError() {
	}

	public NoSolutionError(String message) {
		super(message);
	}
}
