package algo;

import java.util.HashSet;
import java.util.Set;
import model.Client;
import model.Depot;
import model.Instance;
import model.Location;
import model.Route;
import model.Solution;
import model.Vehicule;

/**
 * This generator creates a naive solution. A naive solution attributes one
 * client per vehicle. The chosen location is the cheapest one for the client.
 */
public class NaiveSolver implements GenerativeSolver {

	@Override
	public Solution solve(Instance instance) throws NoSolutionError {
		final Solution solution = new Solution(instance, 0);

		for (Client client : instance.getClients()) {
			// Determine which location we'll deliver the parcel to.
			Location deliveryLocation = this.selectBestLocationForClient(client);

			if (deliveryLocation == null) {
				throw new NoSolutionError("No suitable location for delivery");
			}

			// Create a vehicle to deliver this client.
			Vehicule v = new Vehicule(instance.getDepot(), instance.getVehiculeCapacity(), solution);

			if (!v.addStop(deliveryLocation, client)) {
				throw new NoSolutionError("Could not add a stop for the delivery location");
			}

			// Add this vehicle to the solution
			solution.getVehicules().add(v);
		}

		solution.recalculateCost();
		return solution;
	}

	private Location selectBestLocationForClient(Client client) {
		Location bestLocation = null;
		int bestCost = Integer.MAX_VALUE;

		final Depot depot = client.getInstance().getDepot();
		final Set<Location> possibleLocations = findPossibleLocationsForClient(client);

		for (Location curr : possibleLocations) {
			final int locationCost = depot.getRouteTo(curr).getCost();

			if (locationCost < bestCost) {
				bestCost = locationCost;
				bestLocation = curr;
			}
		}

		return bestLocation;
	}

	private Set<Location> findPossibleLocationsForClient(Client client) {
		final Depot depot = client.getInstance().getDepot();
		final Set<Location> results = new HashSet<>();

		for (Location location : client.getLocations()) {
			// Ignore locations we can't reach from the depot.
			final boolean canGoFromDepotToLoc = depot.hasRouteTo(location);
			final boolean canGoFromLocToDepot = location.hasRouteTo(depot);

			if (!canGoFromDepotToLoc || !canGoFromLocToDepot) {
				continue;
			}

			Route routeToLoc = depot.getRouteTo(location);
			int timeToNextLocation = routeToLoc.getTime();
			int timeToReturnToDepot = location.getRouteTo(depot).getTime();
			int waitTime = 0;

			// Make sure we can make it in time.
			if (timeToNextLocation >= location.getEndTime()) {
				continue;
			}

			// Wait time at the location
			if (timeToNextLocation < location.getStartTime()) {
				waitTime = location.getStartTime() - timeToNextLocation;
			}

			// Make sure we can return to the depot in time
			if ((timeToNextLocation + waitTime + timeToReturnToDepot) >= depot.getEndTime()) {
				continue;
			}

			// If that works, keep the location.
			results.add(location);
		}

		return results;
	}
}
