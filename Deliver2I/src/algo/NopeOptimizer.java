package algo;

import model.Solution;

/**
 * Optimizing solver that doesn't do anything. This is mostly used for testing
 * purposes.
 */
public class NopeOptimizer implements OptimizingSolver {

	@Override
	public void optimize(Solution solution) {
	}
}
