package algo;

import model.Instance;
import model.Solution;

/**
 * Generates a solution to an instance from scratch.
 */
public interface GenerativeSolver {

	/**
	 * Solve the given instance.
	 *
	 * @param instance The instance to solve.
	 * @return A solution to the given instance.
	 * @throws NoSolutionError If no solution to this instance is found.
	 */
	public Solution solve(Instance instance) throws NoSolutionError;
}
