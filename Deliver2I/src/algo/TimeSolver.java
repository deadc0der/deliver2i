/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package algo;

import java.util.HashSet;
import java.util.Set;
import model.Client;
import model.Depot;
import model.Instance;
import model.Location;
import model.Solution;
import model.Vehicule;

/**
 * A generative solver that generates rounds by insertion of locations into
 * rounds. ·
 */
public class TimeSolver implements GenerativeSolver {

	private Set<Client> clientToDeliver;
	private Set<Client> clientDelivered;
	private Set<Location> allLocations;
	private int time;//Time for the current vehicle
	private int waitTime; //Wait time for the current vehicle
	private Depot depot;

	/**
	 * Default constructor
	 */
	public TimeSolver() {
		this.clientToDeliver = new HashSet<>();
		this.clientDelivered = new HashSet<>();
		this.allLocations = new HashSet<>();
		this.time = 0;
	}

	/**
	 * Generating a solution
	 *
	 * @param instance
	 * @return a solution for this instance
	 * @throws NoSolutionError
	 */
	@Override
	public Solution solve(Instance instance) throws NoSolutionError {
		//Initialization
		this.depot = instance.getDepot();
		final Solution solution = new Solution(instance, 0);
		clientToDeliver = new HashSet<>(instance.getClients());//Set contenant les clients à livrer
		Location currentLocation = this.depot;
		Location nextLocation = this.depot;
		findAllLocations();

		//While until all clients are delivered
		while (!this.clientToDeliver.isEmpty()) {
			Vehicule v = new Vehicule(this.depot, instance.getVehiculeCapacity(), solution);
			//While until the current vehicle doesn't return to the depot
			while (nextLocation != null) {
				nextLocation = this.findNextLocation(currentLocation);
				//We need to return to the depot
				if (nextLocation == null) {
					break;
				}
				//Find the client associated to the location
				Client client = nextLocation.getClient();
				if (!v.addStop(nextLocation, client)) {
					break; //Vehicule is full, we need to return to the depot
				}
				this.clientDelivered.add(client);
				this.clientToDeliver.remove(client);

				currentLocation = nextLocation;
			}
			//Add the cost to return to the depot
			v.setCost((v.getCost() + currentLocation.getRouteTo(depot).getCost()));
			//Add the vehicle to the solution
			solution.addVehicule(v);

			//Initialization for the next vehicle
			time = 0;
			currentLocation = this.depot; //The next vehicule starts at the depot
			nextLocation = this.depot;
		}

		//Calculate the solution cost
		solution.recalculateCost();
		return solution;
	}

	/**
	 * Find all locations to deliver
	 */
	private void findAllLocations() {
		for (Client c : clientToDeliver) {
			for (Location l : c.getLocations()) {
				if (!this.allLocations.contains(l)) {
					this.allLocations.add(l);
				}
			}
		}
	}

	/**
	 * Return the next location to deliver
	 *
	 * @param currentLocation
	 * @return the next location to deliver
	 */
	private Location findNextLocation(Location currentLocation) {
		int bestCost = Integer.MAX_VALUE;

		final Location startLocation = currentLocation;
		final Location nextLocation = findBestLocation(startLocation);

		return nextLocation;
	}

	/**
	 * We browse all locations to determine which is the best.
	 *
	 * @param startLocation
	 * @return the best location
	 */
	private Location findBestLocation(Location startLocation) {
		Location bestLocation = null;

		int minCout = Integer.MAX_VALUE;
		int minTime = Integer.MAX_VALUE;
		int minWaitTime = Integer.MAX_VALUE;

		int currentCout = 0;
		int currentTime = 0;

		//Determining the best location
		for (Location l : this.allLocations) {
			//Initialization costs and time to go to this location
			currentCout = startLocation.getRouteTo(l).getCost();
			currentTime = startLocation.getRouteTo(l).getTime();
			waitTime = 0;
			//If locations is valid and the best, we save this location
			if (this.checkLocation(startLocation, l, minCout, minTime, minWaitTime)) {
				bestLocation = l;
				minCout = currentCout;
				minTime = currentTime;
				minWaitTime = waitTime;
			}
		}
		if (bestLocation != null) {
			//The cost of the vehicle is automatically updated when adding the stop
			//Update tour time
			time += minTime + minWaitTime;
		}
		return bestLocation;
	}

	/**
	 * Function used to check the validity of a location according to the
	 * criteria below: - the next location is close enough so that we can return
	 * to the depot after going there - all location customers have not been
	 * delivered - the location has an ideal time window - it has a minimum
	 * cost, minimum time, minimum waiting time while respecting the previous
	 * criteria
	 *
	 * @param startLocation
	 * @param checkedLocation
	 * @param minCout
	 * @param minTime
	 * @param minWaitTime
	 * @return True location is OK / False location is KO
	 */
	private boolean checkLocation(Location startLocation, Location checkedLocation, int minCost, int minTime, int minWaitTime) {
		//Check the cost, the time and the wait time
		boolean costCheck = false;
		boolean timeCheck = false;
		boolean waitTimeCheck = false;

		boolean notDepot = false; //the current location is not the depot
		boolean timeToDepotCheck = false;//we have time to go to the next location and return to the depot
		boolean idealTimeWindow = false;//the location is open when we arrive
		boolean capacityCheck = false;//the vehicle capacity is ok
		boolean clientsAlreadyDelivered = true;//all location customers have been delivered

		//Minimal cost
		int costToNextLocation = startLocation.getRouteTo(checkedLocation).getCost();
		costCheck = costToNextLocation < minCost;

		//Location != depot
		notDepot = !checkedLocation.equals(this.depot);

		//Client delivered ?
		Client client = checkedLocation.getClient();
		clientsAlreadyDelivered = this.clientDelivered.contains(client);

		//Time to go to the next location
		int timeToNextLocation = startLocation.getRouteTo(checkedLocation).getTime();

		//Location schedule is OK
		idealTimeWindow = (this.time + timeToNextLocation) < checkedLocation.getEndTime();
		//Wait time
		if ((this.time + timeToNextLocation) < checkedLocation.getStartTime()) {
			waitTime = checkedLocation.getStartTime() - (this.time + timeToNextLocation);
		}

		//Enough time to go to the next location and return to the depot
		timeToDepotCheck = this.depot.getEndTime() > (this.time + timeToNextLocation + checkedLocation.getRouteTo(this.depot).getTime() + waitTime);

		//Minimal time and minimal wait time
		timeCheck = (timeToNextLocation + waitTime) < minTime;
		waitTimeCheck = waitTime < minWaitTime;

		return costCheck && timeCheck && waitTimeCheck && timeToDepotCheck && !clientsAlreadyDelivered && idealTimeWindow && notDepot;
	}
}
