package algo;

import algo.util.CheckTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Random;
import java.util.Set;
import model.Location;
import model.Solution;
import model.Vehicule;

/**
 * This class is trying to merge two rounds. It is the same operation that the
 * RoundMergingOptimizer, but now we randomly select a merge from the best ten
 * percent of possible merges.
 *
 */
public class RoundMergingRandom implements OptimizingSolver {

	/**
	 * Class witch contains the infos of a merge
	 */
	class MergeInfo {

		int bestR;
		int bestS;
		double gain;

		MergeInfo(int bestR, int bestS, double gain) {
			this.bestR = bestR;
			this.bestS = bestS;
			this.gain = gain;
		}
	}

	/**
	 * Optmize the solution by merging round
	 *
	 * @param solution
	 */
	@Override
	public void optimize(Solution solution) {
		List<Vehicule> vehicules = new ArrayList<>(solution.getVehicules());

		//Improved rounds by merging them
		boolean improved = true;

		while (improved) {
			improved = mergerRound(vehicules);
		}

		solution.setVehicules(new HashSet<>(vehicules));
		solution.recalculateCost();
	}

	/**
	 * Algorithm to merge the rounds
	 *
	 * @param usedVehicles
	 * @return
	 */
	private boolean mergerRound(List<Vehicule> usedVehicles) {
		List<MergeInfo> mergeInfos = new ArrayList<>();

		//Round 1 = R et Round 2 = S
		int bestR = -1;
		int bestS = -1;

		//For all R's vehicles
		for (int r = 0; r < usedVehicles.size(); r++) {
			//For all S's vehicles
			for (int s = 0; s < usedVehicles.size(); s++) {
				//The two rounds are different
				if (r != s) {
					//Calculate the cost of merger
					double gain = mergeCost(usedVehicles.get(r), usedVehicles.get(s));

					//Save results
					MergeInfo mergeInfo = new MergeInfo(r, s, gain);
					mergeInfos.add(mergeInfo);
				}
			}
		}

		//Sort the list by decreasing gain
		Collections.sort(mergeInfos, new Comparator<MergeInfo>() {
			@Override
			public int compare(MergeInfo tc1, MergeInfo tc2) {
				return (tc1.gain < tc2.gain) ? -1 : ((tc1.gain == tc2.gain) ? 0 : 1);
			}
		});

		//If a gain is possible
		if (mergeInfos.get(0).gain >= 0.0) {
			return false;
		}

		//Choose the best ten percent
		if (mergeInfos.size() - 1 > 0) {
			int randomRoundIndex = this.getRandomNumberInRange(0, (int) (mergeInfos.size() * 0.1));
			bestR = mergeInfos.get(randomRoundIndex).bestR;
			bestS = mergeInfos.get(randomRoundIndex).bestS;
		} else if (!mergeInfos.isEmpty()) {
			bestR = mergeInfos.get(0).bestR;
			bestS = mergeInfos.get(0).bestS;
		}

		//Merges the two "best" rounds to merge
		boolean fusion = merger(usedVehicles.get(bestR), usedVehicles.get(bestS));
		//If merger is OK, we can remove the second round which is included in the first round
		if (fusion) {
			usedVehicles.remove(bestS);
		}
		return true;
	}

	/**
	 * Merge v2 in v1
	 *
	 * @return
	 */
	private boolean merger(Vehicule v1, Vehicule v2) {
		int mergeCost = this.mergeCost(v1, v2);
		if (mergeCost > (Integer.MAX_VALUE - 1)) {
			return false;
		}

		//Round cost : costT1 + costT2 + costT1T2  
		v1.setCost((v1.getCost() + (v2.getCost() + mergeCost)));

		//Capacity : capacityV1 + capacityV2
		v1.setRemainingCapacity((v1.getRemainingCapacity() - (v2.getCapacity() - v2.getRemainingCapacity())));

		//Add all stops from T2 to T1
		v1.getStops().addAll(v2.getStops());

		//Clean V2, and remove this from the solution
		v2.clear();
		Set<Vehicule> vehicules = new HashSet<>(v1.getSolution().getVehicules());
		vehicules.remove(v2);
		v1.getSolution().setVehicules(vehicules);

		//Solution cost : cost old solution + cost merger
		//But recalculateCost take care of external vehicles
		v1.getSolution().recalculateCost();

		return true;
	}

	/**
	 * Calculate the cost of the merger v2 in v1
	 *
	 * @return mergeCost Integer.MAX_VALUE when it's impossible to merge the
	 * round
	 *
	 */
	private int mergeCost(Vehicule v1, Vehicule v2) {
		if (v2 == null) {
			return Integer.MAX_VALUE;
		}

		//No stops for the vehicle 1
		if (v1.getStops().isEmpty()) {
			return Integer.MAX_VALUE;
		}

		//No stops for the vehicle 2
		if (v2.getStops().isEmpty()) {
			return Integer.MAX_VALUE;
		}

		//Check the remaining capacity
		if (((v1.getCapacity() - v1.getRemainingCapacity()) + (v2.getCapacity() - v2.getRemainingCapacity())) > v1.getCapacity()) {
			return Integer.MAX_VALUE;
		}

		/**
		 * *****************Check the time********************************
		 */
		CheckTime cT = new CheckTime();
		int timeRound = 0;
		//Depot -> T1
		timeRound = cT.checkTime(v1.getDepot(), v1.getStops().get(0), timeRound);
		if (timeRound == -1) {
			return Integer.MAX_VALUE;
		}

		//T1 time without return to the depot
		for (int i = 0; i < (v1.getStops().size() - 1); i++) {
			timeRound = cT.checkTime(v1.getStops().get(i), v1.getStops().get(i + 1), timeRound);
			if (timeRound == -1) {
				return Integer.MAX_VALUE;
			}
		}

		//Last location from T1 to the first location from T2
		Location lastLocationT1 = v1.getStops().get(v1.getStops().size() - 1);
		Location firstLocationT2 = v2.getStops().get(0);
		timeRound = cT.checkTime(lastLocationT1, firstLocationT2, timeRound);
		if (timeRound == -1) {
			return Integer.MAX_VALUE;
		}

		//T2 time without return to the depot
		for (int i = 0; i < (v2.getStops().size() - 1); i++) {
			timeRound = cT.checkTime(v2.getStops().get(i), v2.getStops().get(i + 1), timeRound);
			if (timeRound == -1) {
				return Integer.MAX_VALUE;
			}
		}

		//T2 -> Depot
		timeRound = cT.checkTime(v2.getStops().get(v2.getStops().size() - 1), v2.getDepot(), timeRound);
		if (timeRound == -1) {
			return Integer.MAX_VALUE;
		}
		/**
		 * **************************************************************
		 */

		/*costMerge = (lastLocation -> firstLocation) - 
                         (lastLocation -> Depot) -
                           (Depot -> firstLocation)*/
		return lastLocationT1.getRouteTo(firstLocationT2).getCost()
				- lastLocationT1.getRouteTo(v1.getDepot()).getCost()
				- v2.getDepot().getRouteTo(firstLocationT2).getCost();
	}

	/**
	 * Generate a random number between min and max
	 *
	 * @param min
	 * @param max
	 * @return
	 */
	private int getRandomNumberInRange(int min, int max) {

		if (min >= max) {
			throw new IllegalArgumentException("max must be greater than min");
		}

		Random r = new Random();
		return r.nextInt((max - min) + 1) + min;
	}

}
