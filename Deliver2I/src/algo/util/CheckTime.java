/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package algo.util;

import model.Location;

/**
 *
 * @author Quentin
 */
public class CheckTime {
    /**
    * Check if the vehicle arrives on time
    * >0 : On time
    * -1 : Late
    * @param from
    * @param to
    * @param timeRound
    * @return 
    */
    public int checkTime(Location from, Location to, int timeRound){
        //Time to go to the next location
        int timeToNextLocation = from.getRouteTo(to).getTime();

        //Location schedule is OK
        boolean idealTimeWindow = (timeRound + timeToNextLocation) < to.getEndTime();
        if(idealTimeWindow){
            //Wait time
            int waitTime = 0;
            if((timeRound + timeToNextLocation) < to.getStartTime()){
                waitTime = to.getStartTime() - (timeRound + timeToNextLocation);
            }
            //Time Round
            timeRound += timeToNextLocation + waitTime;  

        }else{
            return -1;
        }

        return timeRound;
    }
}
