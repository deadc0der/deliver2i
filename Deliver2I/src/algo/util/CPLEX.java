package algo.util;

import ilog.concert.IloException;
import ilog.concert.IloIntExpr;
import ilog.concert.IloIntVar;
import ilog.concert.IloNumExpr;
import ilog.cplex.IloCplex;
import java.util.List;
import model.Client;
import model.Instance;
import model.Location;
import model.Solution;
import model.Vehicule;

/**
 * Utility class that uses the CPLEX Linear Program solver.
 */
public class CPLEX {

	private IloCplex cplex;
	private IloIntVar externalVehicleCountVar;
	private IloIntVar vehicleActiveVar[];
	private List<Vehicule> baseVehicles;

	private boolean clientIsVisitedInVehicle(Client client, Vehicule vehicule) {
		for (Location currStop : vehicule.getStops()) {
			if (currStop.getClient().equals(client)) {
				return true;
			}
		}

		return false;
	}

	private void buildModel(Instance instance) throws IloException {
		cplex = new IloCplex();

		// Decision variables
		externalVehicleCountVar = cplex.intVar(0, Integer.MAX_VALUE, "v");

		vehicleActiveVar = new IloIntVar[baseVehicles.size()];

		for (int r = 0; r < baseVehicles.size(); ++r) {
			vehicleActiveVar[r] = cplex.boolVar(String.format("x" + r));
		}

		// Objective function
		IloNumExpr objectiveSumLines[] = new IloIntExpr[baseVehicles.size()];

		for (int r = 0; r < baseVehicles.size(); ++r) {
			final int cr = baseVehicles.get(r).getCost();
			final IloIntVar xr = vehicleActiveVar[r];
			objectiveSumLines[r] = cplex.prod(cr, xr);
		}

		final int cext = instance.getExternalVehiculeCost();
		cplex.addMinimize(cplex.sum(cplex.sum(objectiveSumLines), cplex.prod(cext, externalVehicleCountVar)));

		// Constraint 1: All clients must be visited at least once
		int i = 0;
		for (Client client : instance.getClients()) {
			IloIntExpr clientSumLines[] = new IloIntExpr[baseVehicles.size()];

			for (int r = 0; r < baseVehicles.size(); ++r) {
				final Vehicule vehicule = baseVehicles.get(r);
				final IloIntVar xr = vehicleActiveVar[r];

				clientSumLines[r] = cplex.prod(xr, clientIsVisitedInVehicle(client, vehicule) ? 1 : 0);
			}

			cplex.addGe(cplex.sum(clientSumLines), 1);
		}

		// Constraint 2: Don't select too many vehicles
		cplex.addLe(cplex.sum(vehicleActiveVar), cplex.sum(instance.getInternalVehiculeCount(), externalVehicleCountVar));
	}

	/**
	 * Solves the given instance by combining the given rounds.
	 * @param instance The instance to solve.
	 * @param vehicules The rounds to combine.
	 * @param maxTimeInSeconds The maximum time allowed combining rounds.
	 * @return The generated solution.
	 */
	private Solution solve(Instance instance, List<Vehicule> vehicules, Integer maxTimeInSeconds) {
		try {
			// Solve the instance
			this.baseVehicles = vehicules;
			buildModel(instance);

			if (maxTimeInSeconds != null) {
				cplex.setParam(IloCplex.DoubleParam.TiLim, maxTimeInSeconds);
			}

			if (!cplex.solve()) {
				return null;
			}

			// Build the solution from CPLEX's answer
			final Solution solution = new Solution(instance, 0);

			for (int r = 0; r < baseVehicles.size(); ++r) {
				if (cplex.getValue(vehicleActiveVar[r]) > 0.5) // Floating point precision be damned
				{
					solution.addVehicule(baseVehicles.get(r));
				}
			}

			solution.recalculateCost();
			return solution;
		} catch (IloException ex) {
			ex.printStackTrace();
			return null;
		}
	}

	private Solution solve(Instance instance, List<Vehicule> vehicules) {
		return solve(instance, vehicules, null);
	}

	/**
	 * Solves the given instance with the given rounds.
	 * @param instance The instance to solve.
	 * @param vehicules The rounds to build the solution out of.
	 * @return The generated solution.
	 */
	public static Solution solveInstance(Instance instance, List<Vehicule> vehicules) {
		return new CPLEX().solve(instance, vehicules);
	}

	/**
	 * Solves the given instance with the given rounds within a time limit.
	 * @param instance The instance to solve.
	 * @param vehicules The rounds to build the solution out of.
	 * @param maxTimeInSeconds The time limit in seconds.
	 * @return The generated solution.
	 */
	public static Solution solveInstance(Instance instance, List<Vehicule> vehicules, int maxTimeInSeconds) {
		return new CPLEX().solve(instance, vehicules, maxTimeInSeconds);
	}
}
