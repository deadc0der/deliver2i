package algo;

import algo.util.CheckTime;
import java.util.ArrayList;
import java.util.List;
import model.Location;
import model.Solution;
import model.Vehicule;

/**
 * This class is trying to change the location order in a round. It checks the
 * cost and the time. In fact, if there is no gain, the order is not change.
 * When there is a gain, this class changes directly the location in the round
 */
public class ChangeLocationOrderOptimizer implements OptimizingSolver {

	/**
	 * Optimize the solution
	 *
	 * @param solution
	 */
	@Override
	public void optimize(Solution solution) {
		List<Vehicule> vehicules = new ArrayList<>(solution.getVehicules());
		int oldCost = solution.getCost();

		//Change the location order in each vehicle's round
		for (Vehicule v : vehicules) {
			changeLocationOrder(v);
		}

		//Recalculate the new solution cost
		solution.recalculateCost();
	}

	/**
	 * Try to change the location order in the vehicle's round
	 */
	private void changeLocationOrder(Vehicule v) {
		for (int i = 0; i < (v.getStops().size() - 1); i++) {
			for (int j = 1; j < v.getStops().size(); j++) {
				if (i != j) {
					//Save the locations
					Location l1 = v.getStops().get(i);
					Location l2 = v.getStops().get(j);
					v.getStops().remove(i);

					//Change the locations    
					if (i > j) {
						v.getStops().remove(j);
						v.getStops().add(j, l1);
						v.getStops().add(i, l2);
					} else {
						v.getStops().remove(j - 1);
						v.getStops().add(i, l2);
						v.getStops().add(j, l1);
					}

					//Check cost and time
					int newCost = v.getTotalCost();
					if ((newCost < v.getCost()) && checkChangeOrder(v)) {
						v.setCost(newCost);
					} else {
						//Reset round
						v.getStops().remove(i);
						if (i > j) {
							v.getStops().remove(j);
							v.getStops().add(j, l2);
							v.getStops().add(i, l1);
						} else {
							v.getStops().remove(j - 1);
							v.getStops().add(i, l1);
							v.getStops().add(j, l2);
						}
					}
				}
			}
		}
	}

	/**
	 * Check if it's possible to change the location order in the round
	 *
	 * @return TRUE : if it's possible to do the round in time FALSE : the
	 * vehicle is late
	 */
	private boolean checkChangeOrder(Vehicule v) {
		CheckTime cT = new CheckTime();
		int timeRound = 0;

		//Depot -> First Location
		timeRound = cT.checkTime(v.getDepot(), v.getStops().get(0), timeRound);
		if (timeRound == -1) {
			return false;
		}

		//All round time without return to the depot
		for (int i = 0; i < (v.getStops().size() - 1); i++) {
			timeRound = cT.checkTime(v.getStops().get(i), v.getStops().get(i + 1), timeRound);
			if (timeRound == -1) {
				return false;
			}
		}

		//Depot -> Last Location
		timeRound = cT.checkTime(v.getStops().get(v.getStops().size() - 1), v.getDepot(), timeRound);
		if (timeRound == -1) {
			return false;
		}

		return true;
	}
}
