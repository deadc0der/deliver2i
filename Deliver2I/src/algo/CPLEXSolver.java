package algo;

import algo.util.CPLEX;
import java.util.*;
import model.*;

/**
 * A generative solver that uses RandomSolver to create rounds, and plugs them
 * into the CPLEX Linear Programming solver to create a solution.
 */
public class CPLEXSolver implements GenerativeSolver {

	static final int NUMBER_OF_RUNS = 10;

	/**
	 * Generates rounds by running RandomSolver multiple times.
	 *
	 * @param instance The instance we're solving.
	 * @return A list of the generated rounds.
	 * @throws NoSolutionError If no solution is found.
	 */
	private List<Vehicule> generateRounds(Instance instance) throws NoSolutionError {
		List<Vehicule> baseRounds = new ArrayList<>();

		for (int i = 0; i < NUMBER_OF_RUNS; ++i) {
			GenerativeSolver baseSolver = new RandomSolver();
			Solution solution = baseSolver.solve(instance);
			baseRounds.addAll(solution.getVehicules());
		}

		return baseRounds;
	}

	@Override
	public Solution solve(Instance instance) throws NoSolutionError {
		return CPLEX.solveInstance(instance, generateRounds(instance));
	}
}
