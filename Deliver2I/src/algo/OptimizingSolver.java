package algo;

import model.Solution;

/**
 * Generates a solution to an instance by improving upon an existing solution.
 */
public interface OptimizingSolver {

	/**
	 * Optimize the given solution, modifying it in-place.
	 *
	 * @param solution The solution to optimize.
	 */
	public void optimize(Solution solution);
}
