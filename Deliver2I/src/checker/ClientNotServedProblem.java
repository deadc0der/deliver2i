package checker;

import model.Client;

/**
 * Problem class to signify that a client hasn't been served.
 * @author jessy
 */
public class ClientNotServedProblem implements Problem {
	final private Client client;

	public ClientNotServedProblem(Client client) {
		this.client = client;
	}
	
	/**
	 * Returns the client that wasn't served.
	 */
	public Client getClient() {
		return client;
	}
	
	@Override
	public String getMessage() {
		return "Client not served: " + client;
	}
}
