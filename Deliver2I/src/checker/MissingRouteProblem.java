package checker;

import model.Location;

/**
 * Problem class to signify a route doesn't exist between 2 points.
 */
public class MissingRouteProblem implements Problem {
	private Location from;
	private Location to;

	public MissingRouteProblem(Location from, Location to) {
		this.from = from;
		this.to = to;
	}

	/**
	 * Returns the location the vehicle was coming from.
	 */
	public Location getFrom() {
		return from;
	}

	/**
	 * Returns the location the vehicle was trying to go to, but couldn't
	 * because there was no appropriate route.
	 */
	public Location getTo() {
		return to;
	}
	
	@Override
	public String getMessage() {
		return String.format("No route from %s to %s", from, to);
	}
}
