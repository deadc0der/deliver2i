package checker;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import model.Client;
import model.Depot;
import model.Instance;
import model.Location;
import model.Solution;
import model.Vehicule;

/**
 * This class can be used to check the validity of a solution.
 */
public class SolutionChecker {

	private Instance instance;
	private Solution solution;

	private SolutionChecker(Solution solution) {
		assert solution != null;
		this.solution = solution;
		this.instance = solution.getInstance();
	}

	/**
	 * Checks the solution the checker was constructed from.
	 *
	 * @return A list of problems with the solution. When the solution is valid,
	 * an empty list is returned.
	 */
	private List<Problem> check() {
		List<Problem> problems = new ArrayList<>();
		Set<Client> clients = new HashSet<>(instance.getClients());

		for (Vehicule vehicule : solution.getVehicules()) {
			// Make sure routes exist from each point to the next
			final Depot depot = instance.getDepot();
			final List<Location> stops = vehicule.getStops();
			int vehiculeTime = depot.getStartTime();

			ensureRouteExists(depot, vehicule.getStops().get(0), problems);
			vehiculeTime = ensureVehiculeArriveOnTime(depot, vehicule.getStops().get(0), problems, vehiculeTime);

			for (int i = 0; i < stops.size(); ++i) {
				boolean idealTimeWindow;
				int waitTime = 0;

				final Location from = stops.get(i);
				final Location to = (i == stops.size() - 1) ? depot : stops.get(i + 1);
				ensureRouteExists(from, to, problems);

				vehiculeTime = ensureVehiculeArriveOnTime(from, to, problems, vehiculeTime);
			}

			// Account for all clients
			for (Location stop : vehicule.getStops()) {
				Client client = stop.getClient();

				// Make sure this client hasn't been served yet
				if (clients.contains(client)) {
					clients.remove(client);
				} else {
					// problems.add(new ClientAlreadyServedProblem(client));
				}
			}
		}

		// Make sure every client was serviced
		if (!clients.isEmpty()) {
			for (Client client : clients) {
				problems.add(new ClientNotServedProblem(client));
			}
		}

		return problems;
	}

	/**
	 * Verifies that a route exists between 2 points.
	 *
	 * @param from The point we're going from.
	 * @param to The point we're going to.
	 * @param problems A problem list. A MissingRouteProblem is added to it if
	 * there is no route between the given points.
	 */
	private void ensureRouteExists(Location from, Location to, List<Problem> problems) {
		if (!from.hasRouteTo(to)) {
			problems.add(new MissingRouteProblem(from, to));
		}
	}

	private int ensureVehiculeArriveOnTime(Location from, Location to, List<Problem> problems, int vehiculeTime) {
		int waitTime = 0;
		boolean idealTimeWindow;

		//Time to go to the next location
		int timeToNextLocation = from.getRouteTo(to).getTime();
		//Location schedule is OK
		idealTimeWindow = (vehiculeTime + timeToNextLocation) < to.getEndTime();
		//Wait time
		if ((vehiculeTime + timeToNextLocation) < to.getStartTime()) {
			waitTime = to.getStartTime() - (vehiculeTime + timeToNextLocation);
		}
		vehiculeTime = timeToNextLocation + waitTime;

		if (!idealTimeWindow) {
			problems.add(new VehiculeLateProblem(to));
		}

		return vehiculeTime;
	}

	/**
	 * Checks the given solution.
	 *
	 * @param solution A solution to check.
	 * @return A list of problems with the solution. When the solution is valid,
	 * an empty list is returned.
	 */
	public static List<Problem> check(Solution solution) {
		SolutionChecker checker = new SolutionChecker(solution);
		return checker.check();
	}
}
