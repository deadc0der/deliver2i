package checker;

import model.Location;

/**
 * Problem class that signifies a vehicle arrived too late at a location.
 */
public class VehiculeLateProblem implements Problem {
	private Location to;

	public VehiculeLateProblem(Location to) {
		this.to = to;
	}

	/**
	 * Returns the location the vehicle arrived late at.
	 */
	public Location getTo() {
		return to;
	}
	
	@Override
	public String getMessage() {
		return String.format("Vehicule arrive too late at %s ",to);
	}
    
}
