package checker;

/**
 * Represents a problem in an instance the checker looked at.
 */
public interface Problem {
	/**
	 * Returns a message that describes the problem.
	 */
	public String getMessage();
}
