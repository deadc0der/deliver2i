package dao;

import java.util.Collection;

/**
 * Data Access Object interface used to access the instance data.
 *
 * @param <T> The type of model object this DAO accesses.
 */
public interface Dao<T> {

	/**
	 * Adds an object to the data store.
	 *
	 * @return A boolean indicating whether the operation was successful.
	 */
	public boolean create(T obj);

	/**
	 * Adds a collection of objects to the data store.
	 *
	 * @return A boolean indicating whether the operation was successful.
	 */
	public boolean create(Collection<T> obj);

	/**
	 * Retrieves an object by its identifier.
	 *
	 * @param id The identifier to search for.
	 * @return The found object, or null if it isn't found.
	 */
	public T findById(Integer id);

	/**
	 * Retrieves all the objects of this type.
	 *
	 * @return A collection of objects.
	 */
	public Collection<T> findAll();

	/**
	 * Updates an object in the data store.
	 *
	 * @param obj The object to update.
	 * @return A boolean indicating whether the operation was successful.
	 */
	public boolean update(T obj);

	/**
	 * Removes an object from the data store.
	 *
	 * @param obj The object to remove.
	 * @return A boolean indicating whether the operation was successful.
	 */
	public boolean delete(T obj);

	/**
	 * Removes all objects of this type from the data store.
	 *
	 * @return A boolean indicating whether the operation was successful.
	 */
	public boolean deleteAll();
	
	/**
	 * Closes the DAO.
	 */
	public void close();
}
