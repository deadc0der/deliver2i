package dao;

import model.Instance;

class JpaInstanceDao extends JpaDao<Instance> implements InstanceDao {
	private JpaInstanceDao(){
		super(Instance.class);
	}
	
	// Singleton ==============================================================
	private static JpaInstanceDao globalInstance = null;
	
	public static JpaInstanceDao getInstance(){
		if( globalInstance == null )
			globalInstance = new JpaInstanceDao();
		
		return globalInstance;
	}
}
