package dao;

public class JpaDaoFactory extends DaoFactory {
	@Override
	public InstanceDao getInstanceDao() {
		return JpaInstanceDao.getInstance();
	}

        public LocationDao getLocationDao() {
            return JpaLocationDao.getInstance();
        }
}
