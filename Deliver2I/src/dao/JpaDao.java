package dao;

import java.util.Collection;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaDelete;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

abstract class JpaDao<T> implements Dao<T> {
	private static final String PERSISTENCE_UNIT = "Deliver2IPU";
	
	private Class<T> objectClass = null;
	private EntityManager entityManager = null;
	
	public JpaDao() {
		EntityManagerFactory emf = Persistence.createEntityManagerFactory(PERSISTENCE_UNIT);
		entityManager = emf.createEntityManager();
	}
	
	public JpaDao(Class<T> objectClass) {
		this();
		assert objectClass != null;
		this.objectClass = objectClass;
	}
	
	@Override
	public boolean create(Object obj) {
		EntityTransaction et = entityManager.getTransaction();

		try {
			et.begin();
			entityManager.persist(obj);
			et.commit();
			return true;
		} catch (Exception ex) {
			et.rollback();
			displayError(ex);
			return false;
		}
	}

	@Override
	public boolean create(Collection<T> obj) {
		EntityTransaction et = entityManager.getTransaction();

		try {
			et.begin();
			
			for(T curr: obj)
				entityManager.persist(curr);
			
			et.commit();
			return true;
		} catch (Exception ex) {
			et.rollback();
			displayError(ex);
			return false;
		}
	}
	
	@Override
	public boolean update(Object obj) {
		EntityTransaction et = entityManager.getTransaction();
		
		try {
			et.begin();
			entityManager.merge(obj);
			et.commit();
			return true;
		} catch (Exception ex) {
			et.rollback();
			displayError(ex);
			return false;
		}
	}

	@Override
	public boolean delete(Object obj) {
		EntityTransaction et = entityManager.getTransaction();
		
		try {
			et.begin();
			entityManager.remove(obj);
			et.commit();
			return true;
		} catch (Exception ex) {
			et.rollback();
			displayError(ex);
			return false;
		}
	}

	@Override
	public T findById(Integer id) {
		return entityManager.find(objectClass, id);
	}

	@Override
	public Collection findAll() {
		CriteriaBuilder cb = entityManager.getCriteriaBuilder();
		CriteriaQuery<T> cq = cb.createQuery(objectClass);
		Root<T> objT = cq.from(objectClass);
		cq.select(objT);
		List<T> allObjT = entityManager.createQuery(cq).getResultList();
		return allObjT;
	}

	@Override
	public boolean deleteAll() {
		EntityTransaction et = entityManager.getTransaction();
		
		try {
			et.begin();

			CriteriaBuilder cb = entityManager.getCriteriaBuilder();
			CriteriaDelete<T> cd = cb.createCriteriaDelete(objectClass);
			entityManager.createQuery(cd).executeUpdate();

			et.commit();
			return true;
		} catch (Exception ex) {
			et.rollback();
			displayError(ex);
			return false;
		}
	}

	@Override
	public void close() {
		if( entityManager != null ){
			entityManager.close();
		}
	}

	protected EntityManager getEntityManager(){
		return entityManager;
	}
	
	private void displayError(Exception ex){
		System.err.println("JPA Exception: " + ex.toString());
		ex.printStackTrace(System.err);
	}
}
