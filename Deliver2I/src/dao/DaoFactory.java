package dao;

/**
 * Factory class used to create DAO's.
 */
public abstract class DaoFactory {
	public abstract InstanceDao getInstanceDao();
	public abstract LocationDao getLocationDao();
	
	public enum PersistenceType {
		JPA,
		Reader,
	}
	
	public static DaoFactory getDaoFactory(PersistenceType persistenceType){
		switch(persistenceType){
			case JPA:
				return new JpaDaoFactory();
				
			case Reader:
				return new ReaderDaoFactory();
			
			default:
				return null;
		}
	}
}
