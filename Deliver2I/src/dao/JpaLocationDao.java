/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import java.util.HashSet;
import java.util.Set;
import javax.persistence.Query;
import model.Client;
import model.Location;

/**
 *
 * @author alexb
 */
public class JpaLocationDao extends JpaDao<Location> implements LocationDao{
    
     private static JpaLocationDao instance;
    
    /**
     * Constructeur par défaut
     */
    private JpaLocationDao(){
        super(Location.class);
    }
    
    static JpaLocationDao getInstance(){
        //Si pas encore d'instance existante on la créée
        if(instance == null){
            instance = new JpaLocationDao();
        }
        return instance;
    }
}
