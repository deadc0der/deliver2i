package dao;

import java.io.File;
import java.io.FileInputStream;
import java.util.*;
import model.Client;
import model.Instance;
import parser.Parser;

public class ReaderInstanceDao implements InstanceDao {
	private List<Instance> instances;
	
	public ReaderInstanceDao(){
		instances = new ArrayList<>();
		File dataDir = new File("../Data");
		
		for(File curr: dataDir.listFiles()){
			if( curr.getName().endsWith("_sol.txt") || !curr.getName().startsWith("instance_") || !curr.getName().endsWith(".txt") )
				continue;
			
			try {
				System.out.println("Parsing: " + curr.getName());
				final Instance instance = Parser.parseInstanceFast(new FileInputStream(curr));
				
				// Give the instance a name so it shows up in the manager
				instance.setName(curr.getName());
				
				// Assign dummy ID's to the clients so the instance view has a key for its color map
				long currentId = 1;
				for(Client client: instance.getClients())
					client.setId(currentId++);

				instances.add(instance);
			} catch (Exception ex) {
				System.err.println("reading " + curr.getName() + ": " + ex.getMessage());
				ex.printStackTrace();
			}
		}
	}
	
	@Override
	public Collection<Instance> findAll() {
		return instances;
	}
	
	@Override
	public boolean create(Instance obj) {
		throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
	}

	@Override
	public boolean create(Collection<Instance> obj) {
		throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
	}

	@Override
	public Instance findById(Integer id) {
		throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
	}

	@Override
	public boolean update(Instance obj) {
		throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
	}

	@Override
	public boolean delete(Instance obj) {
		throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
	}

	@Override
	public boolean deleteAll() {
		throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
	}

	@Override
	public void close() {
	}
}
