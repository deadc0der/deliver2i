package dao;

import model.Instance;

public interface InstanceDao extends Dao<Instance> {
}
