package writer;

import java.io.OutputStream;
import java.io.PrintWriter;
import model.Location;
import model.Solution;
import model.Vehicule;

/**
 * This class writes a solution file.
*/
public class SolutionWriter {
    /**
     * The solution being written.
     */
    private Solution solution;
	
    /**
     * The writer to write to.
     */
    private PrintWriter printWriter;
    
    /**
     * Data constructor
     * @param outStream The stream to write to.
     * @param solution The solution to write.
     */
    public SolutionWriter(OutputStream outStream, Solution solution){
            this.solution = solution;
            this.printWriter = new PrintWriter(outStream);
    }
	
    /**
     * Generate a solution file
     * A line represents a round
     * A line contains the location IDs to deliver
     */
    public void write(){
        for(Vehicule v: solution.getVehicules()){
            for(Location l: v.getStops()){
				// ID 0 is the depot, which should never be in the stops list
				assert l.getFileId() != 0;
                printWriter.print(l.getFileId() + "  ");
            }
			
            printWriter.print("\n");
        }
		
        printWriter.close();
    }
}
