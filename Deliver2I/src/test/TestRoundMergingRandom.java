/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package test;

import algo.NaiveSolver;
import algo.RoundMergingRandom;
import java.io.FileInputStream;
import model.Instance;
import model.Solution;
import parser.Parser;

/**
 *
 * @author Quentin
 */
public class TestRoundMergingRandom {

	public static void main(String[] args) throws Throwable {
		// Read the instance from a file
		final FileInputStream inputStream = new FileInputStream("../Data/instance_0-triangle.txt");
		final Instance instance = Parser.parseInstance(inputStream);
		inputStream.close();
		assert instance != null;

		// Find a solution
		final NaiveSolver solver = new NaiveSolver();
		final Solution solution = solver.solve(instance);
		assert solution != null;
		instance.getSolutions().add(solution);

		final RoundMergingRandom rMr = new RoundMergingRandom();
		rMr.optimize(solution);

	}
}
