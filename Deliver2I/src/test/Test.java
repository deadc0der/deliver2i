package test;

import algo.NaiveSolver;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import model.Instance;
import model.Solution;
import parser.Parser;
import writer.SolutionWriter;

/**
 * 
 * @author jessy
 */
public class Test {

	public static void main(String[] args) throws Throwable {
		// Read the instance from a file
		final FileInputStream inputStream = new FileInputStream("../Data/instance_0-triangle.txt");
		final Instance instance = Parser.parseInstance(inputStream);
		inputStream.close();
		assert instance != null;

		// Find a solution
		final NaiveSolver solver = new NaiveSolver();
		final Solution solution = solver.solve(instance);
		assert solution != null;
		instance.getSolutions().add(solution);

		// Write the solution to a file
		final FileOutputStream outputStream = new FileOutputStream("../Data/solution_naive.txt");
		final SolutionWriter writer = new SolutionWriter(outputStream, solution);
		writer.write();
		outputStream.close();

		// Save the instance and solution to the database
		final EntityManagerFactory emf = Persistence.createEntityManagerFactory("Deliver2IPU");
		final EntityManager em = emf.createEntityManager();

		EntityTransaction et = em.getTransaction();
		et.begin();
		em.persist(instance);
		et.commit();
		em.close();
	}
}
