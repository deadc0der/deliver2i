package test;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import model.Instance;
import parser.Parser;

/**
 * 
 * @author jessy
 */
public class ParserBench {

	static private InputStream makeStreamForFile(String path) throws FileNotFoundException {
		return new BufferedInputStream(new FileInputStream("../" + path));
	}

	static private void tryAllInstances() {
		File dataDir = new File("../Data");

		for (File curr : dataDir.listFiles()) {
			if (curr.getName().endsWith("_sol.txt") || !curr.getName().startsWith("instance_") || !curr.getName().endsWith(".txt")) {
				continue;
			}

			try {
				final Instance instance = Parser.parseInstanceFast(new FileInputStream(curr));
				instance.setName(curr.getName());
				System.out.println(curr.getName() + ": ok");
			} catch (Throwable ex) {
				System.err.println(curr.getName() + ": " + ex.getMessage());
			}
		}
	}

	private static void tryOneInstance(String name) {
		try {
			final Instance instance = Parser.parseInstanceFast(new FileInputStream("../Data/" + name));
			instance.setName(name);
		} catch (Throwable ex) {
			System.err.println(name + ": " + ex.getMessage());
		}
	}

	public static void main(String[] args) throws Exception {
		tryAllInstances();
		// tryOneInstance("instance_16-triangle.txt");
	}
}
