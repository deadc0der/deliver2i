package parser.fast;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import model.Client;
import model.Depot;
import model.Instance;
import model.Location;
import parser.SyntaxError;

/**
 * Utility class used to build the instance as the instance file is read.
 */
public class InstanceBuilder {
	final private List<CustomerInfo> customers;
	final private List<LocationInfo> locations;
	final private List<RouteInfo> routes;
	private int depotId = -1;
	private int vehiculeCapacity = -1;
	private int externalVehiculeCost = -1;
	private int internalVehiculeCount = -1;
	
	/**
	 * Resolve the in-file identifiers to re-build the Instance object.
	 * @return The generated instance object.
	 */
	public Instance buildInstance(){
		// Check the metrics
		if( customers.isEmpty() || locations.isEmpty() || routes.isEmpty() )
			throw new SyntaxError("incomplete file");
		
		if( depotId == -1 || vehiculeCapacity == -1 || externalVehiculeCost == -1 || internalVehiculeCount == -1 )
			throw new SyntaxError("incomplete file");
		
		// Re-build the locations
		Map<Integer, Location> realLocations = new HashMap();
		
		for(LocationInfo curr: locations)
			realLocations.put(curr.id, new Location(curr.x, curr.y, curr.twe, curr.twl, curr.id));
		
		// The depot is a special location
		Location oldDepot = realLocations.get(depotId);
		final Depot depot = new Depot(oldDepot.getX(), oldDepot.getY(), oldDepot.getStartTime(), oldDepot.getEndTime(), oldDepot.getFileId());
		realLocations.put(depotId, depot);
		
		// Re-build the clients
		final Instance instance = new Instance(depot, internalVehiculeCount, externalVehiculeCost, vehiculeCapacity);
		
		for(CustomerInfo curr: customers){
			final Client client = new Client(curr.demand, instance);
			
			for(int locationId: curr.locations)
				client.addLocation(realLocations.get(locationId));
			
			instance.addClient(client);
		}
		
		// Re-build the routes
		for(RouteInfo curr: routes){
			final Location from = realLocations.get(curr.from);
			final Location to = realLocations.get(curr.to);
			from.addDestination(to, curr.cost, curr.time);
		}
		
		return instance;
	}
	
	public InstanceBuilder(){
		customers = new ArrayList<>();
		locations = new ArrayList<>();
		routes = new ArrayList<>();
	}
	
	public void addCustomer(int id, int demand, int [] locations){
		customers.add(new CustomerInfo(id, demand, locations));
	}
	
	public void addLocation(int id, float x, float y, int twe, int twl){
		locations.add(new LocationInfo(id, x, y, twe, twl));
	}
	
	public void addRoute(int from, int to, int cost, int time){
		routes.add(new RouteInfo(from, to, cost, time));
	}

	public void setDepotId(int depotId) {
		this.depotId = depotId;
	}

	public void setVehiculeCapacity(int vehiculeCapacity) {
		this.vehiculeCapacity = vehiculeCapacity;
	}

	public void setExternalVehiculeCost(int externalVehiculeCost) {
		this.externalVehiculeCost = externalVehiculeCost;
	}

	public void setInternalVehiculeCount(int internalVehiculeCount) {
		this.internalVehiculeCount = internalVehiculeCount;
	}

	static class CustomerInfo {
		public int id;
		public int demand;
		public int [] locations;

		public CustomerInfo(int id, int demand, int[] locations) {
			this.id = id;
			this.demand = demand;
			this.locations = locations;
		}
	}

	static class LocationInfo {
		public int id;
		public double x, y;
		public int twe, twl;

		public LocationInfo(int id, float x, float y, int twe, int twl) {
			this.id = id;
			this.x = x;
			this.y = y;
			this.twe = twe;
			this.twl = twl;
		}
	}
	
	static class RouteInfo {
		public int from, to;
		public int cost;
		public int time;

		public RouteInfo(int from, int to, int cost, int time) {
			this.from = from;
			this.to = to;
			this.cost = cost;
			this.time = time;
		}
	}
}
