package parser.fast;

import java.io.IOException;
import java.io.InputStream;
import model.Instance;
import parser.SyntaxError;

/**
 * Fast Instance Parser.
 * This parser is not as robust as the other parser, but it is significantly faster.
 */
public class InstanceParser {
	private int numCustomers = 0;
	private int numLocations = 0;
	
	/**
	 * Reads the file.
	 * @throws java.io.IOException
	 */
	public void read() throws IOException {
		String tagName;
		while((tagName = reader.nextTag()) != null){
			switch (tagName) {
				case "NB_CUSTOMERS":
					numCustomers = reader.readNumber();
					break;
					
				case "NB_LOCATIONS":
					numLocations = reader.readNumber();
					break;
					
				case "NB_VEHICLES":
					builder.setInternalVehiculeCount(reader.readNumber());
					break;
					
				case "VEHICLE_CAPACITY":
					builder.setVehiculeCapacity(reader.readNumber());
					break;
					
				case "EXTERNAL_VEHICLE_COST":
					builder.setExternalVehiculeCost(reader.readNumber());
					break;
					
				case "DEPOT":
					readDepot();
					break;
					
				case "CUSTOMERS":
					readCustomers();
					break;
					
				case "LOCATIONS":
					readLocations();
					break;
					
				case "TRAVEL_COSTS_TIMES":
					readRoutes();
					break;
					
				default:
					throw new SyntaxError("Unrecognized tag: " + tagName);
			}
			
			reader.finishTag(tagName);
		}
	}
	
	/**
	 * Reads a <LOCATIONS> tag
	 */
	private void readLocations(){
		// Skip the header
		reader.readCSVLine();
		
		// Read as many locations as the file promised
		for(int i=0; i < numLocations; ++i){
			String [] fields = reader.readCSVLine();
			
			builder.addLocation(
					Integer.parseInt(fields[0]),
					Float.parseFloat(fields[1]),
					Float.parseFloat(fields[2]),
					Integer.parseInt(fields[3]),
					Integer.parseInt(fields[4]));
		}
	}
	
	/**
	 * Extracts the locations from a CSV line of a <CUSTOMERS> tag.
	 * @param fields The CSV fields.
	 * @param locs The number of locations.
	 * @return The location file ID's.
	 */
	private int [] extractLocations(String [] fields, int locs){
		int [] result = new int[locs];
		
		for(int i=0; i < locs; ++i)
			result[i] = Integer.parseInt(fields[i + 5]);
		
		return result;
	}
	
	/**
	 * Reads a <CUSTOMERS> tag.
	 */
	private void readCustomers(){
		// Skip the header
		reader.readCSVLine();
		
		// Read as many locations as the file promised
		for(int i=0; i < numCustomers; ++i){
			String [] fields = reader.readCSVLine();
			int locs = Integer.parseInt(fields[2]);

			builder.addCustomer(
					Integer.parseInt(fields[0]),
					Integer.parseInt(fields[1]),
					extractLocations(fields, locs));
		}
	}
	
	/**
	 * Reads a <TRAVEL_COSTS_TIMES> tag.
	 */
	private void readRoutes(){
		// Skip the header
		reader.readCSVLine();
		
		// Here we don't know how many there are, so just stop when it looks wrong
		while(true){
			String [] fields = reader.readCSVLine();
			
			if( fields == null )
				break;
			
			builder.addRoute(
					Integer.parseInt(fields[0]), 
					Integer.parseInt(fields[1]),
					Integer.parseInt(fields[2]),
					Integer.parseInt(fields[3]));
		}
	}
	
	/**
	 * Reads a <DEPOT> tag.
	 */
	private void readDepot(){
		// Skip the header
		reader.readCSVLine();
		
		String [] fields = reader.readCSVLine();
		builder.setDepotId(Integer.parseInt(fields[0]));
	}
	
	// Boilerplate
	final private Reader reader;
	final private InstanceBuilder builder;
	
	public InstanceParser(InputStream stream) throws IOException {
		this.reader = new Reader(stream);
		this.builder = new InstanceBuilder();
	}
	
	public Instance getInstance() {
		return builder.buildInstance();
	}
}
