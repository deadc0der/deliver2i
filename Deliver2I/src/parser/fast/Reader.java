package parser.fast;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import parser.SyntaxError;

/**
 * Input abstraction to read from an input stream.
 */
class Reader {
	final private String inputString;
	private int currentPos;
	
	public Reader(InputStream inputStream) throws IOException {
		inputString = readAll(inputStream);
		currentPos = 0;
	}
	
	/**
	 * Reads the entirety of the stream into a string.
	 * @param in The input stream.
	 * @return A string containing all of the stream's contents.
	 * @throws IOException 
	 * 
	 * Taken from https://stackoverflow.com/a/48775964
	 */
	private String readAll(InputStream in) throws IOException {
		try(ByteArrayOutputStream result = new ByteArrayOutputStream()){
			byte [] buffer = new byte[1024];
			int length;
			
			while( (length = in.read(buffer)) != -1 ){
				result.write(buffer, 0, length);
			}
			
			return result.toString();
		}
	}
	
	// Tag reading
	
	/**
	 * Reads the next opening tag and returns its name.
	 * @return The name of the found tag, or null if none is found.
	 */
	public String nextTag(){
		int tagStart = inputString.indexOf('<', currentPos);
		
		if( tagStart == -1 )
			return null;
		
		tagStart += 1;
		final int tagEnd = inputString.indexOf('>', tagStart);
		
		if( tagEnd == -1 )
			throw new SyntaxError("malformed tag");
		
		currentPos = tagEnd + 1;
		return inputString.substring(tagStart, tagEnd);
	}
	
	/**
	 * Reads the next closing tag and verifies it has the correct name.
	 * @param name The name of the tag.
	 */
	public void finishTag(String name){
		String foundTag = nextTag();
		
		if( !foundTag.equals("/" + name)  )
			throw new SyntaxError("malformed tag ending");
	}
	
	// Number reading
	private static boolean isDecimalDigit(char c){
		return (c >= '0') && (c <= '9');
	}
	
	private static int getDecimalDigitValue(char c){
		assert isDecimalDigit(c);
		return c - '0';
	}
	
	public int readNumber(){
		// Skip to the next digit
		while( !isDecimalDigit(inputString.charAt(currentPos)) )
			++currentPos;
		
		// Read each decimal digit until we find something that isn't one
		int result = 0;
		char curr;
		
		while( isDecimalDigit(curr = inputString.charAt(currentPos)) ){
			result = (result * 10) + getDecimalDigitValue(curr);
			currentPos += 1;
		}
		
		return result;
	}
	
	// CSV line reading
	public String [] readCSVLine(){
		if( inputString.charAt(currentPos) == '<' )
			return null;
		
		int startOfLine = currentPos;
		
		// Read until the end of the line
		int endOfLine = inputString.indexOf('\n', currentPos);
		
		if( endOfLine == -1 )
			throw new SyntaxError("Unterminated CSV line");
		
		// Update the position
		currentPos = endOfLine + 1;
		return inputString.substring(startOfLine, endOfLine).trim().split("\t");
	}
}
