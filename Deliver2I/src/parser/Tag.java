package parser;

import java.io.ByteArrayInputStream;
import java.io.IOException;

/**
 * A pseudo-XML tag as read by TagParser.
 */
class Tag {
	/**
	 * The name of the tag.
	 */
	final private String name;
	
	/**
	 * The text contained within the tag.
	 */
	final private String contents;
	
	/**
	 * Constructor.
	 * @param name The name of the tag.
	 * @param contents The text contained within the tag.
	 */
	public Tag(String name, String contents){
		this.name = name;
		this.contents = contents;
	}

	public String getName() {
		return name;
	}

	public String getContents() {
		return contents;
	}
	
	/**
	 * Attempts to read the tag's contents as a decimal number, and returns the
	 * resulting numerical value.
	 * @return The numerical value of the tag's contents.
	 * @throws SyntaxError If the contents of the tag aren't a valid decimal number.
	 */
	public int readAsDecimalNumber() throws SyntaxError {
		try {
			return Integer.parseInt(contents, 10);
		} catch(NumberFormatException ex){
			throw new SyntaxError(String.format("expected number but got \"%s\"", contents));
		}
	}
	
	/**
	 * Attempts to read the tag's contents as a CSV document.
	 * This method creates a CSV parser, whose input is the contents of this
	 * tag, with the given separator character.
	 * @param separator The character that separates fields.
	 * @return The created CSV parser.
	 * @throws SyntaxError If the 
	 * @throws IOException If the stream fails.
	 */
	public CsvParser readAsCSV(char separator) throws IOException {
		ByteArrayInputStream inputStream = new ByteArrayInputStream(contents.getBytes());
		return new CsvParser(inputStream, separator);
	}
}
