package parser;

import java.io.IOException;
import java.io.InputStream;

/**
 * Input stream abstraction.
 * 
 * This class encapsulates the input stream and interprets it as a stream of
 * characters. It provides an interface that allows consuming the stream on a
 * character-by-character basis, while remembering the current character.
 * It also provides some character identification primitives.
 */
final class Input {
	final private InputStream stream;
	private int current;
	
	/**
	 * Constructor.
	 * @param input The stream to read from.
	 * @throws IOException If the stream fails.
	 */
	public Input(InputStream input) throws IOException {
		this.stream = input;
		consume();
	}
	
	/**
	 * Returns the character currently being examined.
	 */
	public char getCurrent() {
		assert current != -1;
		return (char)current;
	}
	
	/**
	 * Consumes the current character and skips to the next.
	 * @throws IOException If the stream fails.
	 */
	public void consume() throws IOException {
		if( current != -1 )
			current = stream.read();
	}

	/**
	 * Determines whether the end of the input stream has been reached.
	 */
	public boolean isEndOfFile(){
		return current == -1;
	}
	
	/**
	 * Determines whether the current character is a decimal digit (0-9).
	 * @return 
	 */
	public boolean isDecimalDigit(){
		return (current >= '0') && (current <= '9');
	}
	
	/**
	 * Calculates the numerical value of the current character as a decimal digit.
	 * It is invalid to call this if the current character isn't guaranteed to be a decimal digit.
	 */
	public int getDecimalDigitValue(){
		assert isDecimalDigit();
		return current - '0';
	}
	
	/**
	 * Determines whether the current character is whitespace, as defined by the
	 * 'isspace' macro from the C Standard Library.
	 * @return 
	 */
	public boolean isWhiteSpace(){
		if( isEndOfFile() )
			return false;
		
		switch(current){
			// 0x0B and 0x0C are vertical tab and form feed, respectively
			case '\t': case '\n': case 0x0B: case 0x0C: case '\r': case ' ':
				return true;
			
			default:
				return false;
		}
	}
	
	/**
	 * Determines whether the current character is acceptable in a tag name.
	 * Acceptable characters contain uppercase letters (A-Z) and the underscore.
	 */
	public boolean isTagNameCharacter(){
		if( isEndOfFile() )
			return false;
		
		return ((current >= 'A') && (current <= 'Z')) || (current == '_');
	}
	
	/**
	 * Ensures the current character is equal to ch.
	 * Checks whether the current character is equal to ch. If it is, the
	 * character is consumed and the function returns. Otherwise, a SyntaxError
	 * exception is thrown and the character is not consumed.
	 * 
	 * @param ch The expected character value.
	 * @throws IOException If the stream fails.
	 * @throws SyntaxError If the character doesn't match expectations.
	 */
	public void expect(char ch) throws IOException, SyntaxError {
		ensureNotEndOfFile();
		
		if( current != ch )
			throw new SyntaxError(String.format("expected '%c' but got '%c'", ch, (char)current));
		
		consume();
	}
	
	/**
	 * Ensures the current character is a decimal digit.
	 * Checks whether the current character is a decimal digit. If it is, the
	 * character is consumed and its numerical value is returned. Otherwise, a
	 * SyntaxError exception si thrown and the character is not consumed.
	 * @return The numerical value of the digit.
	 * @throws IOException If the stream fails.
	 * @throws SyntaxError If the character doesn't match expectations.
	 */
	public int expectDecimalDigit() throws IOException, SyntaxError {
		ensureNotEndOfFile();
		
		if( !isDecimalDigit() )
			throw new SyntaxError(String.format("expected decimal digit but got '%c'", (char)current));
		
		final int digit = getDecimalDigitValue();
		consume();
		return digit;
	}
	
	/**
	 * Ensures the current character is valid in a tag name.
	 * Checks whether the current character is a valid tag name character. If
	 * it is, the character is consumed and returned. Otherwise, a SyntaxError 
	 * exception si thrown and the character is not consumed.
	 * @return The numerical value of the digit.
	 * @throws IOException If the stream fails.
	 * @throws SyntaxError If the character doesn't match expectations.
	 */
	public char expectTagNameCharacter() throws IOException, SyntaxError {
		ensureNotEndOfFile();
		
		if( !isTagNameCharacter() )
			throw new SyntaxError(String.format("expected tag name but got '%c'", (char)current));
		
		final char ch = (char)current;
		consume();
		return ch;
	}
	
	/**
	 * Ensures the current character is a new line.
	 * Checks whether the current character is a newline character. If it is,
	 * the entire newline sequence is consumed and the function returns.
	 * Otherwise, a SyntaxError exception si thrown and the character is not consumed.
	 * 
	 * Note that there are 3 types of possible newline sequences: Unix line endings
	 * consist of a single CR (\n) character, DOS/Windows line endings consist
	 * of a CR-LF sequence (\r\n) and MAcintosh Classic line endings consist of
	 * a single LF (\r) character. This function handles all 3 cases properly
	 * and will consume 1 or 2 characters depending on the type of line ending.
	 * Mixed line endings in the same file are also supported.
	 * 
	 * @throws IOException If the stream fails.
	 * @throws SyntaxError If the character doesn't match expectations.
	 */
	public void expectNewLine() throws IOException, SyntaxError {
		ensureNotEndOfFile();
		
		// If we have a \n then it's a Unix line ending.
		if( current == '\n' ){
			consume();
			return;
		}
		
		// If we have a CR, then it can either be a \r\n Windows line ending or
		// a Macintosh Classic \r line ending.
		if( current == '\r' ){
			consume();
			
			if( current == '\n' )
				consume();
			
			return;
		}
		
		throw new SyntaxError(String.format("expected new line, got '%c'", (char)current));
	}
	
	/**
	 * Ensures the end of the file has not been reached.
	 * Checks if the end of the file has been reached. If it hasn't, the
	 * method returns. Otherwise, a SyntaxError exception is thrown.
	 * @throws SyntaxError if the end of the file has been reached.
	 */
	public void ensureNotEndOfFile() throws SyntaxError {
		if( isEndOfFile() )
			throw new SyntaxError("unexpected end of line");
	}
	
	/**
	 * Skips whitespace from and including the current character.
	 * If the current character is whitespace (as defined by isWhitespace()),
	 * it is consumed as well as any subsequent whitespace characters until
	 * either a non-whitespace character is found, or the end of the file is
	 * encountered.
	 * @throws IOException If the stream fails.
	 */
	public void skipWhitespace() throws IOException {
		while(isWhiteSpace())
			consume();
	}
	
	/**
	 * Collects all characters until endChar is found.
	 * Consumes all characters from the current character until an endChar
	 * character is found. Once it is found, the current character is the
	 * endChar that was found, and all characters up to and not including this
	 * character are returned as a string. If the end of the file occurs before
	 * an endChar is found, a SyntaxError exception is thrown.
	 * @param endChar The character to stop at.
	 * @return All characters up to and not including endChar as a string.
	 * @throws SyntaxError If endChar is not found.
	 * @throws IOException If the stream fails.
	 */
	public String collectAllUntilNext(char endChar) throws IOException, SyntaxError {
		String result = "";
		
		while( current != endChar ){
			if( isEndOfFile() )
				throw new SyntaxError(String.format("expected '%c', got end of file", (char)current));
			
			result = result.concat(Character.toString((char)current));
			consume();
		}
		
		return result;
	}
	
	/**
	 * Collects all characters until an end of line is found.
	 * Consumes all characters from the current character until a newline is
	 * found, and then consumes the newline. Once the newline is found, it is
	 * consumed so the current character is the one immediately after the
	 * newline sequence that was found. All characters up to and not including
	 * the newline sequence are returned as a string. If the end of the file
	 * occurs before a newline is found, a SyntaxError exception is thrown.
	 * @return All characters up to and not including the newline sequence as a string.
	 * @throws SyntaxError If a newline is not found.
	 * @throws IOException If the stream fails.
	 */
	public String collectAllUntilEndOfLine() throws IOException, SyntaxError {
		String result = "";
		
		while( current != '\r' && current != '\n' ){
			if( isEndOfFile() )
				break;
			
			result += Character.toString((char)current);
			consume();
		}
		
		expectNewLine();
		return result;
	}
}
