package parser;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Parser for the form of CSV used in instance files.
 * This CSV form has a particularity that fields may be separated by one or
 * more separator characters, instead of strictly 1 separator as found in most
 * normal CSV files.
 */
class CsvParser {
	private final Input input;
	private final char separator;
	
	/**
	 * A line as read from a CSV file.
	 * This is essentially a wrapper around a simple string map, which maps
	 * field names to their values in the represented line.
	 */
	public static class Line {
		private Map<String,String> fields;

		/**
		 * Constructor.
		 * @param fields The values of the fields on this line. 
		 */
		public Line(Map<String, String> fields) {
			assert fields != null;
			this.fields = fields;
		}
		
		public Map<String, String> getFields() {
			return fields;
		}
		
		/**
		 * Checks whether a given field is present on this line.
		 * @param name The name of the field to check for.
		 * @return true if the field is present, false if not.
		 */
		public boolean hasField(String name){
			return fields.containsKey(name);
		}
		
		/**
		 * Returns the value of the given field or throws if it isn't present.
		 * @param name The name of the field to check for.
		 * @return The value of the field 'name' on this line.
		 * @throws SyntaxError If the field 'name' isn't present on this line.
		 */
		public String requireField(String name) throws SyntaxError {
			if( !hasField(name) )
				throw new SyntaxError(String.format("Missing field '%s' in CSV", name));
			
			return fields.get(name);
		}
		
		/**
		 * Returns the integer value of the given field or throws if it fails.
		 * This method looks for the value of the 'name' field on the line, then
		 * interprets it as an integer and returns the resulting value.
		 * @param name The name of the field to check for.
		 * @return The integer value of the field.
		 * @throws SyntaxError if the field can't be found.
		 * @throws NumberFormatException if the field isn't interpretable as an integer.
		 */
		public int requireIntField(String name) throws SyntaxError, NumberFormatException {
			return Integer.parseInt(requireField(name));
		}
		
		/**
		 * Returns the float value of the given field or throws if it fails.
		 * This method looks for the value of the 'name' field on the line, then
		 * interprets it as a float and returns the resulting value.
		 * @param name The name of the field to check for.
		 * @return The float value of the field.
		 * @throws SyntaxError if the field can't be found.
		 * @throws NumberFormatException if the field isn't interpretable as a float.
		 */
		public float requireFloatField(String name) throws SyntaxError, NumberFormatException {
			return Float.parseFloat(requireField(name));
		}
		
		/**
		 * Returns the double value of the given field or throws if it fails.
		 * This method looks for the value of the 'name' field on the line, then
		 * interprets it as a double and returns the resulting value.
		 * @param name The name of the field to check for.
		 * @return The double value of the field.
		 * @throws SyntaxError if the field can't be found.
		 * @throws NumberFormatException if the field isn't interpretable as a double.
		 */
		public double requireDoubleField(String name) throws SyntaxError, NumberFormatException {
			return Double.parseDouble(requireField(name));
		}
	}
	
	/**
	 * Constructor.
	 * @param inputStream The stream to read from.
	 * @param separator The character that separated fields
	 * @throws IOException If the stream fails.
	 */
	public CsvParser(InputStream inputStream, char separator) throws IOException {
		this.input = new Input(inputStream);
		this.separator = separator;
	}
	
	/**
	 * Reads all lines from the CSV file.
	 * @return An array of all lines found.
	 * @throws IOException If the stream fails.
	 * @throws SyntaxError If the CSV is malformed.
	 */
	public Line[] read() throws IOException, SyntaxError {
		final ArrayList<Line> result = new ArrayList<>();
		
		// Read the first line and memorize the fields and their indices
		final String[] firstLine = readLine();
		
		while(true){
			// Fetch the next line
			String[] line = readLine(firstLine.length);
			
			if( line == null )
				break;
			
			// Read every field in it
			Map<String, String> lineFields = new HashMap<>();
			
			for(int i=0; i < firstLine.length && i < line.length; ++i)
				lineFields.put(firstLine[i], line[i]);
			
			result.add(new Line(lineFields));
		}
		
		// Convert back to an array
		return result.toArray(new Line[0]);
	}
	
	/**
	 * Reads a single line from the CSV file.
	 * @return An array of field values found.
	 * @throws IOException If the stream fails.
	 * @throws SyntaxError If the CSV is malformed.
	 */
	public String[] readLine() throws IOException, SyntaxError {
		// Allow for whitespace at the start of a line
		input.skipWhitespace();
		
		if( input.isEndOfFile() )
			return null;
		
		// Read each field
		final String line = input.collectAllUntilEndOfLine();
		
		// XXX: With normal CSV we would only need to split the line into
		//      separators and that would be all. But some of the instance
		//      files we were given have random chains of separators between
		//      some fields for no apparent reason, so we have to collapse
		//      empty fields.
		return line.split(this.getRegExpForSeparator() + "+");
	}
	
	/**
	 * Reads a single line from the CSV file, with a field count limit.
	 * All fields are read, but the returned array only contains a maximum of
	 * fieldCount elements. If more fields are present, the last fields
	 * aren't separated and count as one single field.
	 * @return An array of field values found.
	 * @throws IOException If the stream fails.
	 * @throws SyntaxError If the CSV is malformed.
	 */
	public String[] readLine(int fieldCount) throws IOException, SyntaxError {
		// Allow for whitespace at the start of a line
		input.skipWhitespace();
		
		if( input.isEndOfFile() )
			return null;
		
		// Read each field
		final String line = input.collectAllUntilEndOfLine();
		
		// XXX: With normal CSV we would only need to split the line into
		//      separators and that would be all. But some of the instance
		//      files we were given have random chains of separators between
		//      some fields for no apparent reason, so we have to collapse
		//      empty fields.
		return line.split(this.getRegExpForSeparator() + "+", fieldCount);
	}
	
	/**
	 * Returns a regular expression that tests for the separator character.
	 * This is necessary because the separator character can be a character
	 * which has a special significance in regular expressions, in which case
	 * it needs to be escaped. This function does the escaping as needed.
	 * @see https://stackoverflow.com/questions/3481828/how-to-split-a-string-in-java
	 */
	private String getRegExpForSeparator(){
		switch(separator){
			case '\\': case '^': case '$': case '.': case '|': case '?':
			case '*': case '+': case '(': case ')': case '[': case '{':
				return String.format("\\%c", separator);
				
			default:
				return Character.toString(separator);
		}
	}
}
