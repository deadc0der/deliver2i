package parser;

import java.io.IOException;
import java.io.InputStream;
import model.Instance;

/**
 * Public interface to the parsers.
 */
public class Parser {
	/**
	 * Parse the given input stream as an instance file.
	 * @param inputStream The stream to read from.
	 * @return The decoded instance.
	 * @throws IOException If the input stream fails.
	 * @throws SyntaxError If the stream data doesn't contain a valid instance file.
	 */
	static public Instance parseInstance(InputStream inputStream) throws IOException, SyntaxError {
		InstanceParser parser = new InstanceParser(inputStream);
		parser.read();
		return parser.getInstance();
	}
	
	/**
	 * Parse the given input stream as an instance file, using the fast parser.
	 * @param inputStream The stream to read from.
	 * @return The decoded instance.
	 * @throws IOException If the input stream fails.
	 * @throws SyntaxError If the stream data doesn't contain a valid instance file.
	 */
	static public Instance parseInstanceFast(InputStream inputStream) throws IOException, SyntaxError {
		parser.fast.InstanceParser parser = new parser.fast.InstanceParser(inputStream);
		parser.read();
		return parser.getInstance();
	}
}
