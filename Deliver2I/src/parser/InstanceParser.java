package parser;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import model.Client;
import model.Depot;
import model.Instance;
import model.Location;

/**
 * Parser for instance files.
 */
class InstanceParser {
	final private TagParser tagParser;
	final private Instance instance;
	
	private Integer customerCount = null;
	private Integer locationCount = null;
	private Integer vehicleCapacity = null;
	private boolean hasVehicleCount = false;
	private boolean hasExternalVehicleCount = false;
	
	private Map<Integer, Location> locations;
	private DepotInfo depot;
	private Set<RouteInfo> routes;
	private Set<ClientInfo> clients;
	
	private static class DepotInfo {
		public int id;
		public int demand;
		public int locationId;
	}
	
	private static class RouteInfo {
		public int fromId;
		public int toId;
		public int cost;
		public int time;
	}
	
	private static class ClientInfo {
		Set<Integer> locations;
		int demand;
	}
	
	/**
	 * Constructor.
	 * Creates the parser with an empty instance.
	 * @param inputStream The stream to read from.
	 * @throws IOException If the stream fails.
	 */
	public InstanceParser(InputStream inputStream) throws IOException {
		this.tagParser = new TagParser(inputStream);
		this.instance = new Instance();
	}
	
	/**
	 * Reads the instance file.
	 * All tags are read and interpreted. In-file ID's are then resolved and
	 * references between locations, vehicles, routes and the depot are
	 * reconstituted.
	 * @throws IOException If the stream fails.
	 * @throws SyntaxError If the instance file is malformed or contains inconsistencies.
	 */
	public void read() throws IOException, SyntaxError {
		// Read all the tags that are found.
		while(true){
			// Get the next tag
			final Tag tag = tagParser.parseTag();
			
			if( tag == null )
				break;
			
			// Interpret the tag
			switch(tag.getName()){
				case "NB_CUSTOMERS":
					readNbCustomers(tag);
					break;
				
				case "NB_LOCATIONS":
					readNbLocations(tag);
					break;
				
				case "NB_VEHICLES":
					readNbVehicles(tag);
					break;
				
				case "VEHICLE_CAPACITY":
					readVehicleCapacity(tag);
					break;
				
				case "EXTERNAL_VEHICLE_COST":
					readExternalVehicleCost(tag);
					break;
				
				case "DEPOT":
					readDepot(tag);
					break;
				
				case "CUSTOMERS":
					readCustomers(tag);
					break;
					
				case "LOCATIONS":
					readLocations(tag);
					break;
					
				case "TRAVEL_COSTS_TIMES":
					readTravelCostTimes(tag);
					break;
					
				default:
					throw new SyntaxError(String.format("Found unknown tag \"%s\"", tag.getName()));
			}
		}
		
		// Make sure all information is present
		if( customerCount == null ) throw new SyntaxError("The NB_CUSTOMERS tag is missing");
		if( locationCount == null ) throw new SyntaxError("The NB_LOCATIONS tag is missing");
		if( vehicleCapacity == null ) throw new SyntaxError("The VEHICLE_CAPACITY tag is missing");
		if( !hasVehicleCount ) throw new SyntaxError("The NB_VEHICLES tag is missing");
		if( !hasExternalVehicleCount ) throw new SyntaxError("The EXTERNAL_VEHICLE_COST tag is missing");
		if( locationCount != locations.size() ) throw new SyntaxError(String.format("The NB_LOCATIONS tag says %d locations, but LOCATIONS has %d", locationCount, locations.size()));
		
		// Resolve all ID references
		resolveReferences();
	}
	
	/**
	 * Retrieves the reconstituted instance.
	 * The instance is only considered valid after a successful call to read()
	 */
	public Instance getInstance() {
		return instance;
	}
	
	/**
	 * Reads a NB_CUSTOMERS tag.
	 */
	private void readNbCustomers(Tag tag) throws SyntaxError {
		if( customerCount != null )
			throw new SyntaxError("Found multiple NB_CUSTOMERS tags");

		customerCount = tag.readAsDecimalNumber();
	}
	
	/**
	 * Reads a NB_LOCATIONS tag.
	 */
	private void readNbLocations(Tag tag) throws SyntaxError {
		if( locationCount != null )
			throw new SyntaxError("Found multiple NB_LOCATIONS tags");
		
		locationCount = tag.readAsDecimalNumber();
	}
	
	/**
	 * Reads a NB_VEHICLES tag.
	 */
	private void readNbVehicles(Tag tag) throws SyntaxError {
		if( this.hasVehicleCount )
			throw new SyntaxError("Found multiple NB_VEHICLES tags");
		
		final int count = tag.readAsDecimalNumber();
		instance.setInternalVehiculeCount(count);
		hasVehicleCount = true;
	}
	
	/**
	 * Reads a VEHICLE_CAPACITY tag.
	 */
	private void readVehicleCapacity(Tag tag) throws SyntaxError {
		if( vehicleCapacity != null )
			throw new SyntaxError("Found multiple VEHICLE_CAPACITY tags");
		
		vehicleCapacity = tag.readAsDecimalNumber();
		instance.setVehiculeCapacity(vehicleCapacity);
	}
	
	/**
	 * Reads a EXTERNAL_VEHICLE_COST tag.
	 */
	private void readExternalVehicleCost(Tag tag) throws SyntaxError {
		if( hasExternalVehicleCount )
			throw new SyntaxError("Found multiple EXTERNAL_VEHICLE_COST tags");
		
		final int cost = tag.readAsDecimalNumber();
		instance.setExternalVehiculeCost(cost);
		hasExternalVehicleCount = true;
	}
	
	/**
	 * Reads a DEPOT tag.
	 */
	private void readDepot(Tag tag) throws SyntaxError, IOException {
		if( depot != null )
			throw new SyntaxError("Found multiple DEPOT tags");
		
		// Read the CSV
		final CsvParser parser = tag.readAsCSV('\t');
		final CsvParser.Line[] lines = parser.read();
		
		// This one is a special case: it has only one line
		if( lines.length != 1 )
			throw new SyntaxError(String.format("Expected 1 depot, got %d", lines.length));
		
		CsvParser.Line curr = lines[0];
		
		// It also cannot have more or less than 1 location
		final int locCount = curr.requireIntField("NBLOC");
		
		if( locCount != 1 )
			throw new SyntaxError(String.format("Depot should have 1 location, has %d", locCount));
		
		// Rebuild the depot
		depot = new DepotInfo();
		depot.id = curr.requireIntField("ID");
		depot.demand = curr.requireIntField("DEMAND");
		depot.locationId = curr.requireIntField("LOCS");
	}
	
	/**
	 * Reads a CUSTOMERS tag.
	 */
	private void readCustomers(Tag tag) throws SyntaxError, IOException {
		if( clients != null )
			throw new SyntaxError("Found multiple CUSTOMERS tags");
		
		clients = new HashSet<>();
		final CsvParser parser = tag.readAsCSV('\t');
		final CsvParser.Line[] lines = parser.read();
		
		for(CsvParser.Line curr: lines){
			ClientInfo client = new ClientInfo();
			client.demand = curr.requireIntField("DEMAND");
			
			final int numLocations = curr.requireIntField("NBLOC");
			String[] locationsIds = curr.requireField("LOCS").split("\t");
			client.locations = new HashSet<>();
			
			for(String locationId: locationsIds)
				client.locations.add(Integer.parseInt(locationId));
			
			clients.add(client);
		}
	}
	
	/**
	 * Reads a LOCATIONS tag.
	 */
	private void readLocations(Tag tag) throws SyntaxError, IOException {
		if( locations != null )
			throw new SyntaxError("Found multiple LOCATIONS tags");
		
		locations = new HashMap<>();
		final CsvParser parser = tag.readAsCSV('\t');
		final CsvParser.Line[] lines = parser.read();
		
		for(CsvParser.Line curr: lines){
			// Read the location's info
			final Location result = new Location(
					curr.requireDoubleField("x"), curr.requireDoubleField("y"), 
					curr.requireIntField("TWE"), curr.requireIntField("TWL"), 0);
			
			// Add it to the list
			final int id = curr.requireIntField("ID");
			result.setFileId(id);
			
			if( locations.containsKey(id) )
				throw new SyntaxError(String.format("Duplicate ID %d found in LOCATIONS tag", id));
			
			locations.put(id, result);
		}
	}
	
	/**
	 * Reads a TRAVEL_COSTS_TIMES tag.
	 */
	private void readTravelCostTimes(Tag tag) throws SyntaxError, IOException {
		if( routes != null )
			throw new SyntaxError("Found multiple TRAVEL_COSTS_TIMES tags");
		
		routes = new HashSet<>();
		final CsvParser parser = tag.readAsCSV('\t');
		final CsvParser.Line[] lines = parser.read();
		
		for(CsvParser.Line curr: lines){
			final RouteInfo route = new RouteInfo();
			route.fromId = curr.requireIntField("IDFROM");
			route.toId = curr.requireIntField("IDTO");
			route.cost = curr.requireIntField("COST");
			route.time = curr.requireIntField("TIME");
			routes.add(route);
		}
	}
	
	/**
	 * Resolves all ID references in the file.
	 * The instance file defines ID's for every element and uses them to encode
	 * relationships between objects. However, since we're eventually storing
	 * the objects in a database, we can't simply re-use these ID's directly as
	 * they might conflict with data that's already in the database. This means
	 * we must store the in-file ID's as we're reading the instance file and
	 * then match every ID to the corresponding object to rebuild the correct
	 * relationships.
	 */
	private void resolveReferences(){
		// Create the actual depot
		final Location depotLocation = findLocationById(this.depot.locationId);
		final Depot depot = new Depot(depotLocation.getX(), depotLocation.getY(), depotLocation.getStartTime(), depotLocation.getEndTime(), this.depot.locationId);
		locations.put(this.depot.locationId, depot);
		instance.setDepot(depot);
		
		// Add routes to the relevant locations
		for(RouteInfo curr: routes){
			Location from = findLocationById(curr.fromId);
			Location to = findLocationById(curr.toId);
			from.addDestination(to, curr.cost, curr.time);
		}
		
		// Create the clients
		for(ClientInfo curr: clients){
			Client client = new Client(curr.demand, instance);
			
			// Add all the locations to the client
			for(Integer locationId: curr.locations){
				Location location = findLocationById(locationId);
				location.setClient(client);
                                client.getLocations().add(location);  
			}
			
			// Add the client to the instance
			instance.addClient(client);
		}
	}
	
	/**
	 * Retrieves a location by its in-file ID.
	 * @param id The in-file ID of the location (NOT database ID).
	 * @return The location found.
	 * @throws SyntaxError if no location with that in-file ID is found.
	 */
	private Location findLocationById(int id) throws SyntaxError {
		if( !locations.containsKey(id) )
			throw new SyntaxError(String.format("location #%d not found", id));
		
		return locations.get(id);
	}
}
