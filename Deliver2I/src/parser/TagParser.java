package parser;

import java.io.IOException;
import java.io.InputStream;

/**
 * Parser for the pseudo-XML tag format used in Instance files.
 * This format is called-pseudo XML because although it resembles XML, it isn't
 * formally specified and presents three differences from XML:
 * 1. It doesn't support XML declaration tags
 * 2. Tag names only contain uppercase letters and underscores
 * 3. Tags can't contain child tags.
 */
class TagParser {
	final private Input input;
	
	/**
	 * Constructor.
	 * @param inputStream The stream from which to read.
	 * @throws IOException If the stream fails.
	 */
	public TagParser(InputStream inputStream) throws IOException {
		this.input = new Input(inputStream);
	}
	
	/**
	 * Parses a single tag.
	 * @return The resulting tag, or null if the end of the file was reached.
	 * @throws IOException If the stream fails.
	 * @throws SyntaxError If the tag is malformed.
	 */
	public Tag parseTag() throws IOException, SyntaxError {
		// Make sure there is a tag
		input.skipWhitespace();
		
		if( input.isEndOfFile() )
			return null;
		
		// Parse the opening tag, and allow for whitespace before the start of
		// the tag's contents.
		final String tagName = parseOpeningTag();
		input.skipWhitespace();
		
		// Read the tag contents
		final String tagContents = input.collectAllUntilNext('<');
		
		// Parse the closing tag
		parseClosingTag(tagName);
		return new Tag(tagName, tagContents);
	}
	
	/**
	 * Parses the opening part of a tag.
	 * @return The name of the tag.
	 * @throws IOException If the stream fails.
	 * @throws SyntaxError If the tag is malformed.
	 */
	private String parseOpeningTag() throws IOException, SyntaxError {
		input.expect('<');
		final String tagName = parseTagName();
		input.expect('>');
		return tagName;
	}
	
	/**
	 * Parses the closing part of a tag.
	 * @param expectedTagName The tag name from the opening tag.
	 * @throws IOException If the stream fails.
	 * @throws SyntaxError If the tag is malformed or doesn't match the expectedTagName.
	 */
	private void parseClosingTag(String expectedTagName) throws IOException, SyntaxError {
		input.expect('<');
		input.expect('/');
		final String tagName = parseTagName();
		input.expect('>');
		
		if( !tagName.equals(expectedTagName) )
			throw new SyntaxError(String.format("Expected closing '%s' tag, found closing '%s' tag", expectedTagName, tagName));
	}
	
	/**
	 * Parses a tag name.
	 * @return The tag name.
	 * @throws IOException
	 * @throws IOException If the stream fails.
	 * @throws SyntaxError If the tag name isn't a valid tag name or is empty.
	 */
	private String parseTagName() throws IOException, SyntaxError {
		// There should be at least one character
		final char initialChar = input.expectTagNameCharacter();
		String tagName = Character.toString(initialChar);
		
		// Then track any remaining characters
		while( input.isTagNameCharacter() ){
			tagName += Character.toString((char)input.getCurrent());
			input.consume();
		}
		
		return tagName;
	}
}
