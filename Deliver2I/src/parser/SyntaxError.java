package parser;

/**
 * Exception thrown when a parser encounters an invalid stream.
 */
public class SyntaxError extends Error {
	public SyntaxError(String msg) {
		super(msg);
	}
}
