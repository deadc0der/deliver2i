package model;

import java.io.Serializable;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

/**
 * The depot from which vehicles start.
 */
@Entity
@DiscriminatorValue("1")
public class Depot extends Location implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * @brief Default ctor.
	 * 
	 * Creates a depot located at (0, 0) with no available time.
	 */
	public Depot() {
	}

	/**
	 * @brief Data ctor.
	 * 
	 * Creates a depot with the given details.
	 * @param x X coordinate of the depot's position.
	 * @param y Y coordinate of the depot's position.
	 * @param startTime The time the depot starts being available.
	 * @param endTime The time the depot stops being available.
	 * @param fileId The ID of this location in the instance file it was read from.
	 */
	public Depot(double x, double y, int startTime, int endTime, long fileId) {
		super(x, y, startTime, endTime, fileId);
	}
	
	// Generated ==============================================================
	@Override
	public String toString() {
		return String.format("model.Depot{location=%s}", super.toString());
	}
}
