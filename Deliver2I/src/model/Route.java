package model;

import java.io.Serializable;
import java.util.Objects;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * @brief A route between 2 locations.
 * 
 * Information about a route that can be taken by a vehicle to travel from a
 * point to another.
 */
@Entity
@Table(name="ROUTE")
public class Route implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="ID")
	private Long id;
	
	/**
	 * What it costs to use this route.
	 */
	@Column(name="COST")
	private int cost;
	
	/**
	 * How long it takes to go across this route (in minutes).
	 */
	@Column(name="TRAVELTIME")
	private int time;
	
	/**
	 * The location this routes starts from.
	 */
	@JoinColumn(name="FROM_LOCATION", referencedColumnName="ID")
	@ManyToOne(optional=false, cascade=CascadeType.ALL)
	private Location fromLocation;
	
	/**
	 * The location this route goes to.
	 */
	@JoinColumn(name="TO_LOCATION", referencedColumnName="ID")
	@ManyToOne(optional=false, cascade=CascadeType.ALL)
	private Location toLocation;

	/**
	 * @brief Default ctor.
	 * 
	 * Creates an invalid route that goes from nowhere to nowhere and takes
	 * 0 time and 0 cost to cross.
	 */
	public Route() {
	}

	/**
	 * @brief Data ctor.
	 * Constructs a route from the given data.
	 * 
	 * @param fromLocation The starting location.
	 * @param toLocation The destination location.
	 * @param cost The cost of using this route.
	 * @param time How long it takes to go across this route (in minutes).
	 */
	public Route(Location fromLocation, Location toLocation, int cost, int time) {
		this();
		this.cost = cost;
		this.time = time;
		this.fromLocation = fromLocation;
		this.toLocation = toLocation;
	}
	
	// Accessors ==============================================================
	public Long getId() {
		return id;
	}

	public int getCost() {
		return cost;
	}

	public void setCost(int cost) {
		this.cost = cost;
	}

	public int getTime() {
		return time;
	}

	public void setTime(int time) {
		this.time = time;
	}

	public Location getFromLocation() {
		return fromLocation;
	}

	public void setFromLocation(Location fromLocation) {
		this.fromLocation = fromLocation;
	}

	public Location getToLocation() {
		return toLocation;
	}

	public void setToLocation(Location toLocation) {
		this.toLocation = toLocation;
	}

	// Generated ==============================================================
	@Override
	public String toString() {
		return String.format("model.Route{id=%d, cost%d, time=%d, from=%s, to=%s}", id, cost, time, fromLocation.getId(), toLocation.getId());
	}

	@Override
	public int hashCode() {
		int hash = 3;
		hash = 61 * hash + this.cost;
		hash = 61 * hash + this.time;
		hash = 61 * hash + Objects.hashCode(this.fromLocation);
		hash = 61 * hash + Objects.hashCode(this.toLocation);
		return hash;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		final Route other = (Route) obj;
		if (this.cost != other.cost) {
			return false;
		}
		if (this.time != other.time) {
			return false;
		}
		if (!Objects.equals(this.fromLocation, other.fromLocation)) {
			return false;
		}
		if (!Objects.equals(this.toLocation, other.toLocation)) {
			return false;
		}
		return true;
	}
}
