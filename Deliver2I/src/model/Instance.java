package model;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

/**
 * @brief An instance of the problem to solve.
 * 
 * Holds all the data that defines an instance of the delivery problem. That is,
 * what clients needs to be delivered, what vehicles we have available, and how
 * much it costs to obtain more.
 */
@Entity
public class Instance implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="ID")
	private Long id;
	
	/**
	 * The depot trucks solving this instance will start from.
	 */
	@ManyToOne(cascade=CascadeType.ALL)
	private Depot depot;
	
	/**
	 * The number of vehicles we have whose cost we can neglect.
	 */
	@Column(name="NB_INTERNAL_VEHICULE")
	private int internalVehiculeCount = 0;
	
	/**
	 * The cost of obtaining an extra vehicle.
	 */
	@Column(name="EXTERNAL_VEHICULE_COST")
	private int externalVehiculeCost = 0;
	
	@Column(name="VEHICULE_CAPACITY")
	private int vehiculeCapacity = 0;
	
	/**
	 * The clients we need to deliver.
	 */
	@OneToMany(mappedBy="instance", cascade=CascadeType.ALL)
	private Set<Client> clients;
	
	/**
	 * All calculated solutions to this particular instance of the problem.
	 */
	@OneToMany(mappedBy="instance", cascade=CascadeType.ALL)
	private Set<Solution> solutions;
	
	@Column(name="NAME")
	private String name;

	/**
	 * @brief Default ctor.
	 * 
	 * Creates an empty instance with no clients, no solutions, no depot, no
	 * internal vehicles, and no depot. External vehicle cost is also 0.
	 */
	public Instance() {
		this.clients = new HashSet<>();
		this.solutions = new HashSet<>();
	}
	
	/**
	 * @brief Data ctor.
	 * 
	 * Creates an instance with the given data.
	 * @param depot The depot from which vehicles start.
	 * @param internalVehiculeCount The number of vehicles we have available.
	 * @param externalVehiculeCost The cost of obtaining an extra vehicle.
	 * 
	 * After construction, the instance still has no clients and no solutions.
	 */
	public Instance(Depot depot, int internalVehiculeCount, int externalVehiculeCost, int vehiculeCapacity) {
		this();
		this.depot = depot;
		this.internalVehiculeCount = internalVehiculeCount;
		this.externalVehiculeCost = externalVehiculeCost;
		this.vehiculeCapacity = vehiculeCapacity;
	}
	
	/**
	 * Add a client to be delivered the instance.
	 * @param client The client to be delivered.
	 * @note You *must* use this rather than manipulate the collection directly.
	 */
	public void addClient(Client client){
		client.setInstance(this);
		clients.add(client);
	}
	
	// Accessors ==============================================================
	public Long getId() {
		return id;
	}

	public Set<Client> getClients() {
		return clients;
	}

	public Set<Solution> getSolutions() {
		return solutions;
	}
	
	public Depot getDepot() {
		return depot;
	}

	public void setDepot(Depot depot) {
		this.depot = depot;
	}

	public int getInternalVehiculeCount() {
		return internalVehiculeCount;
	}

	public void setInternalVehiculeCount(int internalVehiculeCount) {
		this.internalVehiculeCount = internalVehiculeCount;
	}

	public int getExternalVehiculeCost() {
		return externalVehiculeCost;
	}

	public void setExternalVehiculeCost(int externalVehiculeCost) {
		this.externalVehiculeCost = externalVehiculeCost;
	}

	public int getVehiculeCapacity() {
		return vehiculeCapacity;
	}

	public void setVehiculeCapacity(int vehiculeCapacity) {
		this.vehiculeCapacity = vehiculeCapacity;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	// Generated ==============================================================
	@Override
	public String toString() {
		return String.format("model.Instance{id=%d, depot=%s, internalVehiculeCount=%d, externalVehiculeCost=%d, vehiculeCapacity=%d, clients=%s}",
				id, depot, internalVehiculeCount, externalVehiculeCost, vehiculeCapacity, clients);
	}

	@Override
	public int hashCode() {
		int hash = 7;
		hash = 89 * hash + Objects.hashCode(this.depot);
		hash = 89 * hash + this.internalVehiculeCount;
		hash = 89 * hash + this.externalVehiculeCost;
		hash = 89 * hash + this.vehiculeCapacity;
		return hash;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		final Instance other = (Instance) obj;
		if (this.internalVehiculeCount != other.internalVehiculeCount) {
			return false;
		}
		if (this.externalVehiculeCost != other.externalVehiculeCost) {
			return false;
		}
		if (this.vehiculeCapacity != other.vehiculeCapacity) {
			return false;
		}
		if (!Objects.equals(this.depot, other.depot)) {
			return false;
		}
		return true;
	}
}
