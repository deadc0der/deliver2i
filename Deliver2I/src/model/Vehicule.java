package model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OrderColumn;
import javax.persistence.Table;

/**
 * @brief A delivery vehicle.
 * 
 * Represents one vehicle used for delivering to clients. It starts from a
 * depot and visits a set of location to deliver to clients. The vehicle has
 * a cost and a limited capacity.
 */
@Entity
@Table(name="VEHICULE")
public class Vehicule implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="ID")
	private Long id;
	
	/**
	 * The depot the vehicle starts from.
	 */
	@ManyToOne
	@JoinColumn(name="DEPOT", referencedColumnName="ID")
	private Depot depot;
	
	/**
	 * The total capacity of the vehicle.
	 */
	@Column(name="CAPACITY")
	private int capacity = 0;
	
	/**
	 * The capacity of the vehicle that is still available.
	 */
	@Column(name="REMAINING_CAPACITY")
	private int remainingCapacity;
	
	/**
	 * The cost of using this vehicle.
	 * This is the cumulated cost of all routes taken by the vehicle.
	 */
	@Column(name="COST")
	private int cost = 0;
	
	/**
	 * The ordered list of locations this vehicle visits, excluding the depot.
	 */
	@OneToMany(cascade=CascadeType.ALL)
	@OrderColumn(name="position")
	private List<Location> stops;
	
	/**
	 * The solution this vehicle belongs to.
	 */
	@ManyToOne
    @JoinColumn(name="SOLUTION", referencedColumnName="ID")
	private Solution solution;
	
	/**
	 * Default ctor.
	 * Creates a vehicle with no stops, no cost or capacity and no depot that
	 * is attached to no solution.
	 */
	public Vehicule() {
		this.stops = new ArrayList<>();
	}

	/**
	 * Data ctor.
	 * Creates a vehicle from the given data. Note that stops must be added
	 * separately.
	 * @param depot The depot the vehicle starts from
	 * @param capacity The total capacity of this vehicle.
	 * @param solution The solution this vehicle helps implement.
	 */
	public Vehicule(Depot depot, int capacity, Solution solution) {
		this();
		this.depot = depot;
		this.capacity = capacity;
		this.remainingCapacity = capacity;
		this.solution = solution;
	}
	
	/**
	 * Adds a stop for this vehicle.
	 * 
	 * @param loc The location at which to stop.
	 * @param client The client to deliver.
	 * @return true if the stop was added, false if it couldn't be added.
	 */
	public boolean addStop(Location loc, Client client){
		// Make sure we have correct values.
		if( loc == null || client == null )
			return false;
		
		// Make sure we have enough capacity left.
		if( client.getDemand() > remainingCapacity )
			return false;
		
		// Make sure we can get there.
		final Location from = stops.isEmpty() ? depot : stops.get(stops.size() - 1);
		
		if( !from.hasRouteTo(loc) )
			return false;

		// Add the route and subtract the capacity.
		stops.add(loc);
		remainingCapacity -= client.getDemand();
		cost = getTotalCost();
		return true;
	}
        
        public boolean addStop(Location loc, Set<Client> clients) {
            if( loc == null || clients == null )
			return false;
            int totalDemand = 0;
            for(Client c : clients){
                totalDemand += c.getDemand();
            }
            
            if(totalDemand > this.remainingCapacity)
                return false;
            
            final Location from = stops.isEmpty() ? depot : stops.get(stops.size() - 1);
		
            if( !from.hasRouteTo(loc) )
		return false;

		// Add the route and subtract the capacity.
            stops.add(loc);
            remainingCapacity -= totalDemand;
            cost = getTotalCost();
            return true;
        }
	
	/**
	 * Calculates the total cost of this vehicle.
	 * This includes the initial cost, as well as the costs of all routes.
	 */
	public int getTotalCost(){
		// Count the initial cost
		int totalCost = 0;
		
		// If we have stop, add the costs to go to them and back to the depot.
		if( !stops.isEmpty() ){
			// Accumulate the costs to go from the depot to the last location.
			for(int i=0; i < stops.size(); ++i){
				final Location from = (i == 0) ? depot : stops.get(i - 1);
				final Location to = stops.get(i);
				totalCost += from.getRouteTo(to).getCost();
			}
			
			// Add the cost to go back to the depot.
			final Location lastLocation = stops.get(stops.size() - 1);
			totalCost += lastLocation.getRouteTo(depot).getCost();
		}
		
		return totalCost;
	}
	
	/**
	 * Retrieves the currently used capacity.
	 */
	public int getUsedCapacity(){
		return capacity - remainingCapacity;
	}

	/**
	* Reset attributes
	*/
	public void clear(){
		 this.stops.clear();
		 this.capacity = 0;
		 this.cost = 0;
		 this.depot = null;
		 this.solution = null;
	}
	
	@Override
	public Vehicule clone(){
		Vehicule result = new Vehicule(depot, capacity, solution);
		result.remainingCapacity = remainingCapacity;
		result.cost = cost;
		result.stops = new ArrayList<>(stops);
		return result;
	}
	
	// Accessors ==============================================================
	public Long getId() {
		return id;
	}

	public List<Location> getStops() {
		return stops;
	}

        public void setStops(List<Location> stops) {
                this.stops = stops;
        }


	public Depot getDepot() {
		return depot;
	}

	public void setDepot(Depot depot) {
		this.depot = depot;
	}

	public int getCapacity() {
		return capacity;
	}

	public void setCapacity(int capacity) {
		this.capacity = capacity;
	}

	public int getRemainingCapacity() {
		return remainingCapacity;
	}

	public void setRemainingCapacity(int remainingCapacity) {
		this.remainingCapacity = remainingCapacity;
	}

	public int getCost() {
		return cost;
	}

	public void setCost(int cost) {
		this.cost = cost;
	}

	public Solution getSolution() {
		return solution;
	}

	public void setSolution(Solution solution) {
		this.solution = solution;
	}
	
	// Generated ==============================================================
	@Override
	public String toString() {
		return String.format(
				"model.Vehicule{id=%d, capacity=%d, remainingCapacity=%d, cost=%d, stops=%s, depot=%s, solution=%s}",
				id, capacity, remainingCapacity, cost, stops, depot, solution);
	}

	@Override
	public int hashCode() {
		int hash = 5;
		hash = 97 * hash + Objects.hashCode(this.depot);
		hash = 97 * hash + this.capacity;
		hash = 97 * hash + this.remainingCapacity;
		hash = 97 * hash + this.cost;
		hash = 97 * hash + Objects.hashCode(this.stops);
		hash = 97 * hash + Objects.hashCode(this.solution);
		return hash;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		final Vehicule other = (Vehicule) obj;
		if (this.capacity != other.capacity) {
			return false;
		}
		if (this.remainingCapacity != other.remainingCapacity) {
			return false;
		}
		if (this.cost != other.cost) {
			return false;
		}
		if (!Objects.equals(this.depot, other.depot)) {
			return false;
		}
		if (!Objects.equals(this.stops, other.stops)) {
			return false;
		}
		if (!Objects.equals(this.solution, other.solution)) {
			return false;
		}
		return true;
	}
}
