package model;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

/**
 * @brief A client to deliver to.
 * 
 * Represents a client to which we need to deliver some quantity of merchandise.
 * It holds the quantity to deliver, as well as a list of locations to which we
 * can deliver it to. Note that we only need to deliver to one of the locations.
 */
@Entity
public class Client implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="ID")
	private Long id;
	
	/**
	 * The quantity of merchandise to be delivered.
	 */
	@Column(name="DEMAND")
	private int demand = 0;
	
	/**
	 * The set of locations to which we can deliver for the client.
	 */
    @OneToMany(cascade = CascadeType.ALL)
	private Set<Location> locations;
	
	/**
	 * The instance the client belongs to.
	 */
	@ManyToOne
	@JoinColumn(name="INSTANCE", referencedColumnName="ID")
	private Instance instance;
	
	/**
	 * @brief Default ctor.
	 * 
	 * Creates a client with 0 demand and no delivery locations.
	 */
	public Client(){
		this.locations = new HashSet<>();
	}

	/**
	 * @brief Data ctor.
	 * 
	 * Creates a client with the given demand and no delivery locations.
	 * 
	 * @param demand The quantity of merchandise to deliver.
	 * @param instance The instance this client belongs to.
	 */
	public Client(int demand, Instance instance) {
		this();
		this.demand = demand;
		this.instance = instance;
	}
	
	public void addLocation(Location loc){
		assert loc != null;
		locations.add(loc);
		loc.setClient(this);
	}
	
	// Accessors ==============================================================
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Set<Location> getLocations() {
		return locations;
	}
	
	public int getDemand() {
		return demand;
	}

	public void setDemand(int demand) {
		this.demand = demand;
	}

	public Instance getInstance() {
		return instance;
	}

	public void setInstance(Instance instance) {
		this.instance = instance;
	}
	
	// Generated ==============================================================
	@Override
	public int hashCode() {
		int hash = 3;
		hash = 11 * hash + this.demand;
		hash = 11 * hash + Objects.hashCode(this.locations);
		return hash;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		final Client other = (Client) obj;
		if (this.demand != other.demand) {
			return false;
		}
		if (!Objects.equals(this.locations, other.locations)) {
			return false;
		}
		if (!Objects.equals(this.instance, other.instance)) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return String.format("model.Client{id=%d, demand=%d, locations=%s}", id, demand, locations);
	}
}
