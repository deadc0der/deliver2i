package model;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

/**
 * A solution to an instance of the problem.
 */
@Entity
public class Solution implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="ID")
	private Long id;
	
	/**
	 * The instance this solution solves.
	 */
	@OneToOne
	@JoinColumn(name="INSTANCE")
	private Instance instance;
	
	/**
	 * The total cost of this solution.
	 */
	@Column(name="COST")
	private int cost;
	
	/**
	 * The vehicles used by this solution.
	 */
	@OneToMany(cascade=CascadeType.ALL)
	private Set<Vehicule> vehicules;

	/**
	 * @brief Default ctor.
	 * Creates an invalid solution with 0 cost, linked to no instance, that
	 * uses no vehicles.
	 */
	public Solution() {
		this.vehicules = new HashSet<>();
	}

	/**
	 * @brief Data ctor.
	 * Creates a solution from the given data. Note that the vehicles must still
	 * be added separately.
	 * @param instance The instance this solution solves.
	 * @param cost The total cost of this solution.
	 */
	public Solution(Instance instance, int cost) {
		this();
		this.instance = instance;
		this.cost = cost;
	}
	
	public void recalculateCost(){
		int result = 0;
		
		// Count the cost of each vehicle
		for(Vehicule v: vehicules)
			result += v.getTotalCost();
		
		// Add external vehicle costs
		final int externalVehicleCount = Math.max(vehicules.size() - instance.getInternalVehiculeCount(), 0);
		result += externalVehicleCount * instance.getExternalVehiculeCost();
		
		this.cost = result;
	}
	
	public boolean addVehicule(Vehicule v){
		return this.vehicules.add(v);
	}

	@Override
	public Solution clone() throws CloneNotSupportedException {
		Solution result = new Solution(instance, cost);
		
		for(Vehicule curr: vehicules)
			result.vehicules.add(curr.clone());
		
		return result;
	}
    
	
	
	// Accessors ==============================================================
	public Long getId() {
		return id;
	}

	public Instance getInstance() {
		return instance;
	}

	public void setInstance(Instance instance) {
		this.instance = instance;
	}

	public int getCost() {
		return cost;
	}

	public void setCost(int cost) {
		this.cost = cost;
	}

	public Set<Vehicule> getVehicules() {
		return vehicules;
	}

        public void setVehicules(Set<Vehicule> vehicules) {
            this.vehicules = vehicules;
        }
        
	// Generated ==============================================================
	@Override
	public String toString() {
		return String.format("model.Solution{id=%d, cost=%d}", id, cost);
	}

	@Override
	public int hashCode() {
		int hash = 3;
		hash = 41 * hash + Objects.hashCode(this.instance);
		hash = 41 * hash + this.cost;
		return hash;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		final Solution other = (Solution) obj;
		if (this.cost != other.cost) {
			return false;
		}
		if (!Objects.equals(this.instance, other.instance)) {
			return false;
		}
		return true;
	}
}
