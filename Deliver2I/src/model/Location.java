package model;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorType;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MapKey;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * @brief A physical location.
 * 
 * The location of a delivery point or the depot.
 */
@Entity
@Table(name="LOCATION")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "LOCATIONTYPE", discriminatorType = DiscriminatorType.INTEGER)
@DiscriminatorValue("0")
public class Location implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="ID")
	private Long id;
	
	/**
	 * The ID of the location in the instance file.
	 */
	private long fileId;
	
	/**
	 * X coordinate of the location's position.
	 */
	@Column(name="X")
	private double x = 0;
	
	/**
	 * Y coordinate of the location's position.
	 */
	@Column(name="Y")
	private double y = 0;
	
	/**
	 * Time at which this location starts being available, in minutes from the
	 * start of the delivery round. Inclusive.
	 */
	@Column(name="STARTTIME")
	private int startTime = 0;
	
	/**
	 * Time at which this location stops being available, in minutes from the
	 * start of the delivery round. Exclusive.
	 */
	@Column(name="ENDTIME")
	private int endTime = 0;
	
	/**
	 * Map of locations that can be reached from this one.
	 */
	@OneToMany(cascade=CascadeType.ALL, mappedBy="fromLocation")
	@MapKey(name="toLocation")
	private Map<Location, Route> routes;
        
        @ManyToOne(cascade=CascadeType.ALL)
        @JoinColumn(name="CLIENT", referencedColumnName="ID")
        private Client client;
	
	/**
	 * @brief Default ctor.
	 * 
	 * Creates a location located at (0, 0) with no available time and from
	 * which no other locations can be reached.
	 */
	public Location() {
		this.routes = new HashMap<>();
	}
	
	/**
	 * @brief Data constructor.
	 * 
	 * Creates an initialized location.
	 * 
	 * @param x The x coordinate of the location's position
	 * @param y The y coordinate of the location's position
	 * @param startTime The time the location starts being available, in minutes from the start of the delivery round.
	 * @param endTime The time the location stops being available, in minutes from the start of the delivery round.
	 * @param fileId The ID of this location in the instance file it was read from.
	 * 
	 * After construction, the location still has no accessible locations, these
	 * must be added using addDestination()
	 */
	public Location(double x, double y, int startTime, int endTime, long fileId) {
		this();
		assert startTime <= endTime;
		
		this.x = x;
		this.y = y;
		this.startTime = startTime;
		this.endTime = endTime;
		this.fileId = fileId;
	}
	
	/**
	 * Retrieves the duration of this location's availability.
	 * @return The duration in minutes.
	 */
	public int getAvailableDuration(){
		return endTime - startTime;
	}
	
	/**
	 * Looks for a route from this location to loc
	 * @param loc The destination location.
	 * @return The route if one is found, or null if none is found.
	 * 
	 * @note This method can only found routes to locations added using addDestinations().
	 */
	public Route getRouteTo(Location loc){
		return this.routes.get(loc);
	}
	
	/**
	 * Determines whether loc is accessible from this location.
	 * @param loc The destination locations.
	 * @return true if a route is found, false if none if found.
	 */
	public boolean hasRouteTo(Location loc){
		return this.routes.containsKey(loc);
	}
	
	/**
	 * Adds a location to the list of locations accessible from this location.
	 * @param loc The destination location, must not be null.
	 * @param cost How much it costs to go from this location to loc.
	 * @param time How long (in minutes) it takes to go from this location to loc.
	 * @return true if the destination was added, false if it already existed.
	 */
	public boolean addDestination(Location loc, int cost, int time){
		assert loc != null;
		Route r = new Route(this, loc, cost, time);
		return (this.routes.put(loc, r) != null);
	}
	
	/**
	 * Sets the position of this location in one call.
	 * @param x The x coordinate of the location's position
	 * @param y The y coordinate of the location's position
	 * @note This is equivalent to {@code setX(x); setY(y);}
	 */
	public void setXY(double x, double y){
		this.x = x;
		this.y = y;
	}
	
	// Accessors ==============================================================
	public Long getId() {
		return id;
	}
	
	public double getX() {
		return x;
	}

	public void setX(double x) {
		this.x = x;
	}

	public double getY() {
		return y;
	}

	public void setY(double y) {
		this.y = y;
	}

	public int getStartTime() {
		return startTime;
	}

	public void setStartTime(int startTime) {
		this.startTime = startTime;
	}

	public int getEndTime() {
		return endTime;
	}

	public void setEndTime(int endTime) {
		this.endTime = endTime;
	}

	public Map<Location, Route> getRoutes() {
		return routes;
	}

	public long getFileId() {
		return fileId;
	}

	public void setFileId(long fileId) {
		this.fileId = fileId;
	}

        public Client getClient() {
            return client;
        }

        public void setClient(Client client) {
            this.client = client;
        }
        
        

	// Generated ==============================================================
	@Override
	public int hashCode() {
		int hash = 3;
		hash = 13 * hash + (int) (Double.doubleToLongBits(this.x) ^ (Double.doubleToLongBits(this.x) >>> 32));
		hash = 13 * hash + (int) (Double.doubleToLongBits(this.y) ^ (Double.doubleToLongBits(this.y) >>> 32));
		hash = 13 * hash + this.startTime;
		hash = 13 * hash + this.endTime;
		return hash;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		final Location other = (Location) obj;
		if (Double.doubleToLongBits(this.x) != Double.doubleToLongBits(other.x)) {
			return false;
		}
		if (Double.doubleToLongBits(this.y) != Double.doubleToLongBits(other.y)) {
			return false;
		}
		if (this.startTime != other.startTime) {
			return false;
		}
		if (this.endTime != other.endTime) {
			return false;
		}
		return true;
	}
	
	@Override
	public String toString() {
		return String.format("model.Location{id=%d, position=(%f, %f), startTime=%d, endTime=%d, fileId=%d}", 
				id, x, y, startTime, endTime, fileId);
	}
}
