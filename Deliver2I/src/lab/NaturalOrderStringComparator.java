package lab;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

/**
 * A String comparator that sorts in natural order. This means, for example,
 * that the string "12" will be sorted after "2"
 */
public class NaturalOrderStringComparator implements Comparator<String> {
	private static boolean isDecimalDigit(int c) {
		return (c >= '0') && (c <= '9');
	}

	/**
	 * Reads a number from the given string. Unlike Integer.parseInt, this
	 * method stops at the first non-number character, rather than throw an
	 * exception.
	 *
	 * @param s The string to read from.
	 * @return The resulting number.
	 */
	private static int readNumber(String s) {
		int result = 0;

		for (int i = 0; i < s.length(); ++i) {
			char c = s.charAt(i);

			if (!isDecimalDigit(c)) {
				break;
			}

			result = result * 10 + (c - '0');
		}

		return result;
	}

	/**
	 * Returns the character at the given position, or -1 if it doesn't exist.
	 *
	 * @param s The string to read from.
	 * @param i The index of the character?
	 * @return The character, or -1 if the index is out of bounds.
	 */
	private static int getChar(String s, int i) {
		return i < s.length() ? s.charAt(i) : -1;
	}

	@Override
	public int compare(String lhs, String rhs) {
		int maxLength = Math.max(lhs.length(), rhs.length());

		for (int i = 0; i < maxLength; ++i) {
			int leftCh = getChar(lhs, i), rightCh = getChar(rhs, i);

			// If both characters are the same, compare the next set
			if (leftCh == rightCh) {
				continue;
			}

			// If one is a digit, and the other one isn't, then the digit is higher
			if (isDecimalDigit(leftCh) && isDecimalDigit(rightCh)) {
				// If both are decimal digits, compare their numeric values
				final int leftNum = readNumber(lhs.substring(i));
				final int rightNum = readNumber(rhs.substring(i));
				return leftNum - rightNum;
			} else if (isDecimalDigit(leftCh)) {
				// If only one of them is a decimal digit, then it is superior
				return +1;
			} else if (isDecimalDigit(rightCh)) {
				// If only one of them is a decimal digit, then it is superior
				return -1;
			} else {
				// Non-digits are compared normally
				return leftCh - rightCh;
			}
		}

		return 0;
	}

	public static void main(String[] args) {
		String strings[] = {
			"instance-0.txt",
			"instance-1.txt",
			"instance-2.txt",
			"instance-3.txt",
			"instance-4.txt",
			"instance-5.txt",
			"instance-6.txt",
			"instance-7.txt",
			"instance-8.txt",
			"instance-9.txt",
			"instance-10.txt",
			"instance-11.txt",
			"instance-12.txt",
			"instance-13.txt",
			"instance-14.txt",
			"instance-15.txt",
			"instance-16.txt",
			"instance-17.txt",
			"instance-18.txt",
			"instance-19.txt",
			"instance-20.txt",
			"instance-21.txt",
			"instance-22.txt",
			"instance-23.txt",
			"instance-24.txt",
			"instance-25.txt",
			"instance-26.txt",
			"instance-27.txt",
			"instance-28.txt",
			"instance-29.txt",
			"instance-30.txt",
			"instance-31.txt",
			"instance-32.txt",
			"instance-33.txt",
			"instance-34.txt",
			"instance-35.txt",
			"instance-36.txt",
			"instance-37.txt",
			"instance-38.txt",
			"instance-39.txt",};

		// Make a list from the array so we can sort it
		List<String> test = Arrays.asList(strings);

		// Shuffle the order around
		test.sort((o1, o2) -> {
			double k = Math.random();
			if (k < 0.4) {
				return -1;
			}
			if (k > 0.6) {
				return +1;
			}
			return 0;
		});

		// Sort it again
		test.sort(new NaturalOrderStringComparator());

		for (int i = 0; i < test.size(); ++i) {
			System.out.format("[%d]\t%s\n", i, test.get(i));
		}
	}
}
