package lab;

/**
 * Controller for the loading window displayed as the UI loads.
 */
public class SplashController {
	private SplashFrame splashFrame;
	
	public SplashController(){
		this.splashFrame = new SplashFrame();
	}
	
	public void open(){
		this.splashFrame.setVisible(true);
	}
	
	public void close(){
		this.splashFrame.setVisible(false);
	}
	
	public void update(float progress, String message){
		this.splashFrame.setProgress(progress, message);
	}
}
