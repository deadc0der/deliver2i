package lab;

import lab.manager.ManagerController;
import dao.DaoFactory;
import dao.InstanceDao;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import lab.bench.BenchController;
import main.CommandLine;

/**
 * General controller of the Lab UI.
 * This controller manages the state of the entire application.
 */
public class AppController {
	private Params params = null;
	private InstanceDao instanceDao = null;
	private ManagerController instanceManager = null;
	private BenchController bench = null;
	
	private AppController(Params params){
		this.params = params;
	}

	public InstanceDao getInstanceDao() {
		return instanceDao;
	}
	
	public void run(){
		// Display a loading screen
		SplashController splash = new SplashController();
		splash.update(0, "Lancement de l'application");
		splash.open();
		
		// Obtain our DAO
		splash.update(0.5f, "Connexion à la BDD");
		DaoFactory daoFactory = DaoFactory.getDaoFactory(params.persistenceType);
		this.instanceDao = daoFactory.getInstanceDao();
		
		// Open the instance manager
		if( params.openManager ){
			splash.update(1.0f, "Lancement");
			instanceManager = new ManagerController(this);
			splash.close();
			instanceManager.show();
		}
		
		// Open the bench
		if( params.openBench ){
			bench = new BenchController(this);
			bench.open();
		}
	}
	
	public static void main(String[] args) {
		// Make the app play nice with the system on mac
		try {
			System.setProperty("apple.laf.useScreenMenuBar", "true");
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (ClassNotFoundException | IllegalAccessException | InstantiationException | UnsupportedLookAndFeelException ex) {
			Logger.getLogger(AppController.class.getName()).log(Level.SEVERE, null, ex);
			System.exit(-1);
		}
		
		// Read options from the command line
		try {
			CommandLine cmdLine = new CommandLine(args);
			AppController app = new AppController(readParams(cmdLine));
			app.run();
		} catch(Exception ex){
			Logger.getLogger(AppController.class.getName()).log(Level.SEVERE, null, ex);
			JOptionPane.showMessageDialog(null, "Exception: " + ex.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
		}
	}
	
	/**
	 * Contains the parameters of the application, as read from the command line.
	 */
	private static class Params {
		/**
		 * The type of persistence to use for the DAO.
		 */
		public DaoFactory.PersistenceType persistenceType;
		
		/**
		 * Whether the manager window should be opened.
		 */
		public boolean openManager;
		
		/**
		 * Whether the Bench (solver tester) 
		 */
		public boolean openBench;
	};
	
	/**
	 * Reads the command line.
	 * @param cl Command line to read.
	 * @return A corresponding params object.
	 */
	private static Params readParams(CommandLine cl){
		Params p = new Params();
		p.persistenceType = getPersistenceType(cl);
		p.openManager = !cl.hasOption("no-manager");
		p.openBench = !cl.hasOption("no-bench");
		return p;
	}
	
	private static DaoFactory.PersistenceType getPersistenceType(CommandLine cl){
		switch(cl.getOption("dao", "")){
			default:
			case "reader" : return DaoFactory.PersistenceType.Reader;
			case "jpa" : return DaoFactory.PersistenceType.JPA;
		}
	}
}
