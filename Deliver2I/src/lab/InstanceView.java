package lab;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Polygon;
import java.awt.geom.Rectangle2D;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.swing.JComponent;
import model.Client;
import model.Depot;
import model.Instance;
import model.Location;
import model.Solution;
import model.Vehicule;

/**
 * Displays an instance and its solution.
 */
public class InstanceView extends JComponent {

	/**
	 * Stores info about a given vehicule.
	 */
	private static class VehiculeInfo {

		/**
		 * The color in which this round is displayed.
		 */
		public Color color;
	}

	private Instance instance;
	private Vector2 minPoint, maxPoint;
	private Vector2 padding;
	private Map<Long, Color> clientColors;
	private Solution solution;
	private Map<Vehicule, VehiculeInfo> vehiculeInfo;

	public InstanceView() {
	}

	public Instance getInstance() {
		return instance;
	}

	/**
	 * Sets the instance to be displayed.
	 * @param instance The instance to display.
	 */
	public void setInstance(Instance instance) {
		this.instance = instance;

		// The solution was for the previous instance, so remove them.
		this.setSolution(null);

		if (instance != null) {
			// Calculate the dimensions of the new instance
			this.padding = new Vector2(10, 10);
			updateDimensions();

			// Decide which colors to use for each client
			selectClientColors();
		}

		repaint();
	}

	/**
	 * Sets the solution to be displayed.
	 * @param solution The solution to display.
	 */
	public void setSolution(Solution solution) {
		this.solution = solution;

		if (solution != null) {
			Map<Vehicule, VehiculeInfo> newInfo = new HashMap<>();

			for (Vehicule vehicule : solution.getVehicules()) {
				VehiculeInfo vi = new VehiculeInfo();
				vi.color = generateColor();
				newInfo.put(vehicule, vi);
			}

			vehiculeInfo = newInfo;
		}
	}

	@Override
	protected void paintComponent(Graphics graphics) {
		Graphics2D g = (Graphics2D) graphics;
		final Dimension size = this.getSize();

		// Make sure we have an instance
		if (instance == null) {
			final String placeholderText = "No Instance";
			final Rectangle2D extents = g.getFontMetrics().getStringBounds(placeholderText, g);

			g.setColor(Color.white);
			g.fillRect(0, 0, size.width, size.height);
			g.setColor(Color.black);
			g.drawString(placeholderText,
					(int) (size.width - extents.getWidth()) / 2,
					(int) (size.height - extents.getHeight()) / 2);

			return;
		}

		// Draw the background
		g.setColor(Color.white);
		g.fillRect(0, 0, this.getSize().width, this.getSize().height);

		// Draw the origin cross
		final Vector2 origin = convertCoords(0, 0);
		g.setColor(new Color(0xE0E0E0));
		g.drawLine((int) origin.x, 0, (int) origin.x, size.height);
		g.drawLine(0, (int) origin.y, size.width, (int) origin.y);

		// Draw each client's locations
		for (Client client : instance.getClients()) {
			g.setColor(clientColors.get(client.getId()));

			for (Location location : client.getLocations()) {
				Vector2 center = convertCoords(location.getX(), location.getY());
				g.fillRect((int) center.x - 4, (int) center.y - 4, 8, 8);
			}
		}

		// Draw the solution, if we have one
		if (solution != null) {
			g.setColor(Color.black);
			g.setStroke(new BasicStroke(2));

			final Depot depot = solution.getInstance().getDepot();

			for (Vehicule vehicule : solution.getVehicules()) {
				final VehiculeInfo info = vehiculeInfo.get(vehicule);
				g.setColor(info.color);

				final List<Location> stops = vehicule.getStops();
				assert vehicule.getStops().size() > 0;

				// Draw a line from the depot to the first location
				drawSolutionLine(g, depot, stops.get(0), stops.size() > 1);

				// Draw the lines between locations back to the depot
				for (int i = 0; i < stops.size(); ++i) {
					final Location from = stops.get(i);
					final Location to = (i == stops.size() - 1) ? depot : stops.get(i + 1);
					drawSolutionLine(g, from, to, stops.size() > 1);
				}
			}
		}
	}

	/**
	 * Draws a line from a solution.
	 * @param g The graphics context to draw in.
	 * @param from The location the line starts at.
	 * @param to The location the line ends at.
	 * @param drawArrow Whether or not to draw an arrow along the line.
	 */
	private void drawSolutionLine(Graphics2D g, Location from, Location to, boolean drawArrow) {
		final Vector2 fromCoords = convertCoords(from.getX(), from.getY());
		final Vector2 toCoords = convertCoords(to.getX(), to.getY());
		g.drawLine((int) fromCoords.x, (int) fromCoords.y, (int) toCoords.x, (int) toCoords.y);

		if (drawArrow) {
			final double ARROW_SIZE = 8;

			final Vector2 dir = new Vector2(toCoords.x - fromCoords.x, toCoords.y - fromCoords.y).normalized();
			final Vector2 center = new Vector2((fromCoords.x + toCoords.x) / 2, (fromCoords.y + toCoords.y) / 2);
			final Vector2 right = dir.perp();
			final Vector2 left = right.scaled(-1);

			Polygon triangle = new Polygon();
			triangle.addPoint((int) (center.x + dir.x * ARROW_SIZE), (int) (center.y + dir.y * ARROW_SIZE));
			triangle.addPoint((int) (center.x + right.x * ARROW_SIZE), (int) (center.y + right.y * ARROW_SIZE));
			triangle.addPoint((int) (center.x + left.x * ARROW_SIZE), (int) (center.y + left.y * ARROW_SIZE));
			g.fillPolygon(triangle);
		}
	}

	/**
	 * Recalculates the scale and offsets to adapt to resizing.
	 */
	private void updateDimensions() {
		double minX = Double.POSITIVE_INFINITY;
		double minY = Double.POSITIVE_INFINITY;
		double maxX = Double.NEGATIVE_INFINITY;
		double maxY = Double.NEGATIVE_INFINITY;

		// We need to find the locations with the highest and lowest dimensions
		for (Client client : instance.getClients()) {
			for (Location location : client.getLocations()) {
				if (location.getX() < minX) {
					minX = location.getX();
				}
				if (location.getY() < minY) {
					minY = location.getY();
				}
				if (location.getX() > maxX) {
					maxX = location.getX();
				}
				if (location.getY() > maxY) {
					maxY = location.getY();
				}
			}
		}

		minPoint = new Vector2(minX, minY);
		maxPoint = new Vector2(maxX, maxY);
	}

	/**
	 * Select random colors for each client.
	 */
	private void selectClientColors() {
		final Map<Long, Color> newColors = new HashMap<>();

		for (Client client : instance.getClients()) {
			assert client.getId() != null;
			newColors.put(client.getId(), generateColor());
		}

		this.clientColors = newColors;
	}

	/**
	 * Converts the given map coordinates to view coordinates.
	 * @param x The X coordinate.
	 * @param y The Y coordinate.
	 * @return The corresponding vector in view coordinates.
	 */
	Vector2 convertCoords(double x, double y) {
		final Vector2 result = new Vector2();
		result.x = (x - minPoint.x) * (getSize().width - 2 * padding.x) / (maxPoint.x - minPoint.x) + padding.x;
		result.y = (y - minPoint.y) * (getSize().height - 2 * padding.y) / (maxPoint.y - minPoint.y) + padding.y;
		return result;
	}

	/**
	 * Converts the given map size to view size.
	 * @param x The X coordinate.
	 * @param y The Y coordinate.
	 * @return The corresponding vector in view size.
	 */
	Vector2 convertSize(double w, double h) {
		final Vector2 virtualSize = getVirtualSize();
		final Vector2 actualSize = new Vector2(getSize().width - padding.x * 2, getSize().height - padding.y * 2);

		final Vector2 result = new Vector2();
		result.x = w * actualSize.x / virtualSize.x;
		result.y = h * actualSize.y / virtualSize.y;
		return result;
	}
	
	/**
	 * Returns the size of the map, in map coordinates.
	 */
	private Vector2 getVirtualSize() {
		return new Vector2(maxPoint.x - minPoint.x, maxPoint.y - minPoint.y);
	}

	/**
	 * Returns a random color.
	 */
	private static Color generateColor() {
		return new Color(
				(float) Math.random(),
				(float) Math.random(),
				(float) Math.random()
		);
	}
}
