package lab.bench;

import algo.GenerativeSolver;
import algo.SolverFactory;

/**
 * Information about a generator.
 */
public class GeneratorInfo {
	final private SolverFactory.Generator kind;
	final private String name;

	public GeneratorInfo(SolverFactory.Generator kind, String name) {
		this.kind = kind;
		this.name = name;
	}

	public SolverFactory.Generator getKind() {
		return kind;
	}
	
	public GenerativeSolver getSolver() {
		try {
			return SolverFactory.createGenerator(kind);
		} catch(SolverFactory.GeneratorNotFound err){
			// This should never happen, since we create these from the enum
			return null;
		}
	}

	public String getName() {
		return name;
	}

	@Override
	public String toString() {
		return name;
	}
}
