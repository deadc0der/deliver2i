package lab.bench;

import java.util.List;
import javax.swing.table.AbstractTableModel;
import model.Instance;
import model.Solution;

/**
 * TableModel implementation to display a list of instances.
 */
public class InstanceTableModel extends AbstractTableModel {
	private List<InstanceInfo> instances;

	public InstanceTableModel(List<InstanceInfo> instances) {
		this.instances = instances;
	}

	@Override
	public int getRowCount() {
		return instances.size();
	}

	@Override
	public int getColumnCount() {
		return 9;
	}

	@Override
	public String getColumnName(int columnIndex) {
		switch(columnIndex){
			case 0: return "Name";
			case 1: return "State";
			case 2: return "Cost";
			case 3: return "Véhicules";
			case 4: return "Time";
			case 5: return "Valid";
			case 6: return "∆ cost";
			case 7: return "∆ vehicules";
			case 8: return "optim. time";
			default: return null;
		}
	}

	@Override
	public Class<?> getColumnClass(int columnIndex) {
		return Object.class;
	}

	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		final InstanceInfo instanceInfo = instances.get(rowIndex);
		final Instance instance = instanceInfo.getInstance();
		final Solution solution = instanceInfo.getSolution();
		
		switch(columnIndex){
			case 0:
				return instance.getName() != null ? instance.getName() : "(no name)";
				
			case 1:
				return instanceInfo.getState().toString();
				
			case 2:
				return solution == null ? "n/a" : solution.getCost();
				
			case 3:
				return solution == null ? "n/a" : String.format("%d/%d", solution.getVehicules().size(), instance.getInternalVehiculeCount());
				
			case 4:
				return solution == null ? "n/a" : instanceInfo.getTime() + "ms";
				
			case 5:
				return solution == null ? "n/a" : instanceInfo.isValid() ? "yes" : "no";
				
			case 6:
				return instanceInfo.getDeltaCost();
				
			case 7:
				return instanceInfo.getDeltaVehicles();
				
			case 8:
				return instanceInfo.getOptiTime() + "ms";
				
			default:
				return null;
		}
	}

	@Override
	public boolean isCellEditable(int rowIndex, int columnIndex) {
		return false;
	}

	// We don't support editing, so the next methods are useless
	@Override
	public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
	}
}
