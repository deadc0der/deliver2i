package lab.bench;

import model.Instance;
import model.Solution;

/**
 * Information about an instance.
 */
public class InstanceInfo {
	/**
	 * State in which the instance currently is.
	 */
	public static enum State {
		/** The instance hasn't been solved yet */
		NotStarted,
		
		/** The instance is being solved */
		Running,
		
		/** The instance has been solved */
		Solved,
		
		/** The instance is being optimized */
		Optimizing,
		;
		
		@Override		
		public String toString() {
			switch(this){
				case NotStarted: return "not started";
				case Running: return "solving";
				case Solved: return "finished";
				case Optimizing: return "optimizing";
				default: return null;
			}
		}
	}
	
	private Instance instance;
	private Solution solution;
	private State state;
	private long time;
	private boolean valid;
	private long optiTime;
	private int deltaVehicles;
	private int deltaCost;

	public InstanceInfo(Instance instance) {
		this.instance = instance;
		this.solution = null;
		this.state = State.NotStarted;
		this.time = 0;
	}

	public Instance getInstance() {
		return instance;
	}

	public Solution getSolution() {
		return solution;
	}

	public State getState() {
		return state;
	}

	public void setState(State state) {
		this.state = state;
	}

	public void setSolution(Solution solution) {
		this.solution = solution;
	}

	public long getTime() {
		return time;
	}

	public void setTime(long time) {
		this.time = time;
	}

	public boolean isValid() {
		return valid;
	}

	public void setValid(boolean valid) {
		this.valid = valid;
	}

	public int getDeltaVehicles() {
		return deltaVehicles;
	}

	public void setDeltaVehicles(int deltaVehicles) {
		this.deltaVehicles = deltaVehicles;
	}

	public int getDeltaCost() {
		return deltaCost;
	}

	public void setDeltaCost(int deltaCost) {
		this.deltaCost = deltaCost;
	}

	public long getOptiTime() {
		return optiTime;
	}

	public void setOptiTime(long optiTime) {
		this.optiTime = optiTime;
	}
	
	public void reset(){
		this.solution = null;
		this.state = State.NotStarted;
	}
}
