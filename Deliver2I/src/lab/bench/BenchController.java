package lab.bench;

import algo.GenerativeSolver;
import algo.NoSolutionError;
import algo.OptimizingSolver;
import algo.SolverFactory;
import checker.Problem;
import checker.SolutionChecker;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.swing.SwingWorker;
import lab.AppController;
import lab.NaturalOrderStringComparator;
import model.Instance;
import model.Solution;
import writer.SolutionWriter;

/**
 * Controller class for the bench (solver tester) window.
 * This allows the user to test various solvers and optimizers.
 */
public class BenchController implements BenchWindow.Delegate {
	private BenchWindow window;
	private AppController controller;
	private boolean running = false;
	
	private List<GeneratorInfo> solvers;
	private List<OptimizerInfo> optimizers;
	private List<InstanceInfo> instances;

	/**
	 * Constructor.
	 * @param controller The AppController launching this window.
	 */
	public BenchController(AppController controller) {
		this.controller = controller;
		this.window = new BenchWindow(this);
		refreshSolverList();
		refreshInstanceList();
	}
	
	public void open(){
		window.setVisible(true);
	}
	
	public void close(){
		window.setVisible(false);
	}
	
	/**
	 * Reads the list of solvers and optimizers and updates the combo boxes.
	 */
	private void refreshSolverList(){
		List<GeneratorInfo> newSolvers = new ArrayList<>();
		
		for(SolverFactory.Generator curr: SolverFactory.Generator.values()){
			try {
				final GenerativeSolver solver = SolverFactory.createGenerator(curr);
				newSolvers.add(new GeneratorInfo(curr, curr.toString()));
			} catch(SolverFactory.GeneratorNotFound ex){
				JOptionPane.showMessageDialog(window, String.format("Generator '%s' not found: %s", curr, ex.getMessage()), "Generator not found", JOptionPane.ERROR_MESSAGE);
			}
		}
		
		this.solvers = newSolvers;
		this.window.setSolvers(newSolvers);
		
		List<OptimizerInfo> newOptimizers= new ArrayList<>();
		
		for(SolverFactory.Optimizer curr: SolverFactory.Optimizer.values()){
			try {
				final OptimizingSolver solver = SolverFactory.createOptimizer(curr);
				newOptimizers.add(new OptimizerInfo(curr, curr.toString()));
			} catch(SolverFactory.OptimizerNotFound ex){
				JOptionPane.showMessageDialog(window, String.format("Optimizer '%s' not found: %s", curr, ex.getMessage()), "Generator not found", JOptionPane.ERROR_MESSAGE);
			}
		}
		
		this.optimizers = newOptimizers;
		this.window.setOptimizers(newOptimizers);
	}
	
	/**
	 * Reloads the list of instances from the DAO and updates the list.
	 */
	private void refreshInstanceList(){
		final Collection<Instance> allInstances = controller.getInstanceDao().findAll();
		final List<InstanceInfo> newInstances = new ArrayList<>();
		
		for(Instance instance: allInstances)
			newInstances.add(new InstanceInfo(instance));
		
		Comparator<String> cmp = new NaturalOrderStringComparator();
		newInstances.sort((o1, o2) -> {
			return cmp.compare(o1.getInstance().getName(), o2.getInstance().getName());
		});
		
		this.instances = newInstances;
		this.window.setInstances(newInstances);
	}
	
	/**
	 * Find the directory in which to store solutions for the given generator.
	 * @param info Info about the generator.
	 * @return The directory where the solutions should be stored.
	 */
	private File getSolutionsDirectory(GeneratorInfo info){
		// Find the solutions directory, creating it if necessary
		File solutionsDir = new File("../Solutions");
		
		if( solutionsDir.exists() && !solutionsDir.isDirectory() )
			throw new Error("Solutions exists but is not a directory");
		
		if( !solutionsDir.exists() )
			solutionsDir.mkdirs();
		
		// Create a subdirectory with the current timestamp
		final long timestamp = new Date().getTime();
		final String dirName = String.format("%s-%d", info.getName(), timestamp);
		
		final File result = new File("../Solutions/" + dirName);
		
		if( result.exists() )
			throw new Error("The solutions directory already exists");
		
		// Create it if necessary, then return it
		result.mkdirs();
		return result;
	}
	
	@Override
	public void onStartRunning(GeneratorInfo info) {
		// Don't start more than one run at once.
		if( running ){
			JOptionPane.showMessageDialog(window, "Can't start when solvers are still running");
			return;
		}
		
		try {
			(new SwingWorker<Void,Void>() {
				@Override
				protected Void doInBackground() throws Exception {
					try {
						File solutionsDir = getSolutionsDirectory(info);
						runSolver(info, solutionsDir);
					} catch(Throwable ex){
						Logger.getLogger(BenchController.class.getName()).log(Level.SEVERE, null, ex);
					}
					return null;
				}
			}).execute();
		} catch(Throwable ex){
			Logger.getLogger(BenchController.class.getName()).log(Level.SEVERE, null, ex);
		}
	}
	
	/**
	 * Writes the given solution to the given directory.
	 * @param solutionsDir The directory in which to save the file.
	 * @param solution The solution to write.
	 * @param instance The instance the solution solves.
	 * @throws IOException
	 */
	private void saveSolution(File solutionsDir, Solution solution, Instance instance) throws IOException {
		// Rebuild the name of the solution file
		final String instanceName = instance.getName();
		final String solutionName = instanceName.substring(0, instanceName.lastIndexOf(".")) + "_sol.txt";
		
		// Create the file where it will be written
		final String solutionPath = solutionsDir.getPath() + "/" + solutionName;
		final File solutionFile = new File(solutionPath);
		solutionFile.createNewFile();
		
		// Write the solution to the file
		FileOutputStream stream = new FileOutputStream(solutionFile);
		SolutionWriter writer = new SolutionWriter(stream, solution);
		writer.write();
	}
	
	/**
	 * Runs the given solver on every instance and saves the solutions.
	 * @param info Information about the solver to run.
	 * @param solutionsDir The directory where the solutions should be stored.
	 */
	private void runSolver(GeneratorInfo info, File solutionsDir){
		// Mark the solvers as running
		running = true;

		for(InstanceInfo curr: instances)
			curr.reset();
		
		window.setStartAllowed(false);
		window.refreshInstanceStatus();
		
		for(InstanceInfo instanceInfo: instances){
			// Mark the instance as running
			instanceInfo.setState(InstanceInfo.State.Running);
			window.refreshInstanceStatus();

			try {
				// Run the solver
				final GenerativeSolver solver = (GenerativeSolver) info.getSolver();
				final long startTime = System.currentTimeMillis();
				final Solution solution = solver.solve(instanceInfo.getInstance());
				final long endTime = System.currentTimeMillis();
				
				// Update the instance info
				instanceInfo.setSolution(solution);
				instanceInfo.setTime(endTime - startTime);
				
				// Check the solution
				List<Problem> problems = SolutionChecker.check(solution);
				instanceInfo.setValid(problems.isEmpty());
				
				if( !problems.isEmpty() ){
					String msg = String.format("Solution for instance '%s' is invalid:\n", instanceInfo.getInstance().getName());
					
					for(Problem curr: problems){
						msg += " - " + curr.getMessage() + "\n";
					}
					
					JOptionPane.showMessageDialog(window, msg);
				} else {
					// If the solution was valid, save it
					try {
						saveSolution(solutionsDir, solution, instanceInfo.getInstance());
					} catch(IOException ex){
						JOptionPane.showMessageDialog(window, "Failed to write solution for instance " + instanceInfo.getInstance().getName() + ": " + ex.getMessage());
					}
				}
			} catch(NoSolutionError err){
				JOptionPane.showMessageDialog(window, "No solution found for instance #" + instanceInfo.getInstance().getId());
			}

			instanceInfo.setState(InstanceInfo.State.Solved);
			window.refreshInstanceStatus();
		}

		running = false;
		window.setStartAllowed(true);
		window.setOptimizeAllowed(true);
	}

	@Override
	public void onStartOptimizing(OptimizerInfo info) {
		// Don't start more than one run at once.
		if( running ){
			JOptionPane.showMessageDialog(window, "Can't start when solvers are still running");
			return;
		}
		
		try {
			(new SwingWorker<Void,Void>() {
				@Override
				protected Void doInBackground() throws Exception {
					try {
						runOptimizer(info);
					} catch(Throwable ex){
						Logger.getLogger(BenchController.class.getName()).log(Level.SEVERE, null, ex);
					}
					return null;
				}
			}).execute();
		} catch(Throwable ex){
			Logger.getLogger(BenchController.class.getName()).log(Level.SEVERE, null, ex);
		}
	}
	
	/**
	 * Runs the given optimizer on every instance and saves the solutions.
	 * @param info Information about the optimizer to run.
	 */
	private void runOptimizer(OptimizerInfo info){
		// Mark the solvers as running
		running = true;
		
		window.setOptimizeAllowed(false);
		window.refreshInstanceStatus();
		
		for(InstanceInfo instanceInfo: instances){
			// Mark the instance as running
			instanceInfo.setState(InstanceInfo.State.Optimizing);
			window.refreshInstanceStatus();

			int oldCost = instanceInfo.getSolution().getCost();
			int oldVehicles = instanceInfo.getSolution().getVehicules().size();

			// Run the solver
			final OptimizingSolver solver = info.getSolver();
			final long startTime = System.currentTimeMillis();
			solver.optimize(instanceInfo.getSolution());
			final long endTime = System.currentTimeMillis();

			// Update the instance info
			instanceInfo.setOptiTime(endTime - startTime);
			instanceInfo.setDeltaCost(instanceInfo.getSolution().getCost() - oldCost);
			instanceInfo.setDeltaVehicles(instanceInfo.getSolution().getVehicules().size() - oldVehicles);

			// Check the solution
			List<Problem> problems = SolutionChecker.check(instanceInfo.getSolution());
			instanceInfo.setValid(problems.isEmpty());

			if( !problems.isEmpty() ){
				String msg = String.format("Solution for instance '%s' is invalid:\n", instanceInfo.getInstance().getName());

				for(Problem curr: problems){
					msg += " - " + curr.getMessage() + "\n";
				}

				JOptionPane.showMessageDialog(window, msg);
			}
			
			instanceInfo.setState(InstanceInfo.State.Solved);
			window.refreshInstanceStatus();
		}

		running = false;
		window.setOptimizeAllowed(true);
	}

	@Override
	public void onReset() {
		refreshSolverList();
		refreshInstanceList();
	}
}
