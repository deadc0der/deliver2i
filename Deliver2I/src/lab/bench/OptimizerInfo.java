package lab.bench;

import algo.OptimizingSolver;
import algo.SolverFactory;

/**
 * Information about an optimizer.
 * @author jessy
 */
public class OptimizerInfo {
	final private SolverFactory.Optimizer kind;
	final private String name;

	public OptimizerInfo(SolverFactory.Optimizer kind, String name) {
		this.kind = kind;
		this.name = name;
	}

	public SolverFactory.Optimizer getKind() {
		return kind;
	}
	
	public OptimizingSolver getSolver(){
		try {
			return SolverFactory.createOptimizer(kind);
		} catch(SolverFactory.OptimizerNotFound err){
			return null;
		}
	}

	public String getName() {
		return name;
	}

	@Override
	public String toString() {
		return name;
	}
}
