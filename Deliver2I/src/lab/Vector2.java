package lab;

import java.awt.Dimension;

/**
 * Represents a 2-dimensional vector with real coordinates.
 */
class Vector2 {

	public double x = 0, y = 0;

	public Vector2() {
	}

	public Vector2(Dimension d) {
		this.x = (double) d.width;
		this.y = (double) d.height;
	}

	public Vector2(double x, double y) {
		this.x = x;
		this.y = y;
	}

	/**
	 * Calculates the length (or norm) of the vector.
	 */
	public double length() {
		return Math.sqrt(x * x + y * y);
	}

	/**
	 * Returns a copy of the vector reduced to unit length.
	 * @return 
	 */
	public Vector2 normalized() {
		final double len = length();
		return new Vector2(x / len, y / len);
	}

	/**
	 * Returns a copy of the vector multiplied by the given scalar k.
	 */
	public Vector2 scaled(double k) {
		return new Vector2(k * x, k * y);
	}

	/**
	 * Returns a vector that is perpendicular to this one.
	 */
	public Vector2 perp() {
		return new Vector2(y, -x);
	}

	@Override
	public String toString() {
		return String.format("Coords{x=%f, y=%f}", x, y);
	}
}
