package lab.manager;

import model.Instance;

/**
 * Controller for the instance viewing window.
 */
public class InstanceViewerController {
	private Instance instance;
	private InstanceViewerWindow window;

	public InstanceViewerController(Instance instance) {
		this.instance = instance;
		this.window = new InstanceViewerWindow(instance);
	}
	
	public void show(){
		window.setVisible(true);
	}
}
