package lab.manager;

import javax.swing.tree.DefaultMutableTreeNode;
import model.Instance;

/**
 * TreeNode implementation to display instances.
 */
class InstanceTreeNode extends DefaultMutableTreeNode {
	final private Instance instance;

	public InstanceTreeNode(Instance instance) {
		super(instance);
		this.instance = instance;
	}

	@Override
	public String toString() {
		return String.format("#%d %s", instance.getId(), instance.getName());
	}

	public Instance getInstance() {
		return instance;
	}
}
