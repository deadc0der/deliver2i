package lab.manager;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.FileSystems;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.SwingWorker;
import javax.swing.filechooser.FileNameExtensionFilter;
import lab.AppController;
import lab.SplashController;
import model.Instance;
import model.Solution;
import parser.Parser;
import parser.SyntaxError;

/**
 * Controller for the Instance Manager window.
 */
public class ManagerController implements ManagerWindow.Delegate {
	private final AppController controller;
	private final ManagerWindow window;
	private final ImportProgressWindow importProgress;
	
	public ManagerController(AppController controller){
		this.controller = controller;
		this.window = new ManagerWindow(this);
		this.importProgress = new ImportProgressWindow();
	}
	
	public void show(){
		window.setVisible(true);
		onRefresh();
	}

	@Override
	public void onRefresh() {
		window.setInstances(controller.getInstanceDao().findAll());
	}

	@Override
	public void onImportSingleFile() {
        // Ask the user to select a file to import
		JFileChooser chooser = new JFileChooser();
		chooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
		chooser.setCurrentDirectory(FileSystems.getDefault().getPath(".").toFile());
		chooser.setFileFilter(new FileNameExtensionFilter("Instance Files", "txt"));
		
		if( chooser.showOpenDialog(window) != JFileChooser.APPROVE_OPTION )
			return;
		
		// Import the file
		final List<File> files = new ArrayList<>();
		files.add(chooser.getSelectedFile());
		importInstanceFiles(files);
	}

	@Override
	public void onImportDirectory() {
        // Ask the user to select a file to import
		JFileChooser chooser = new JFileChooser();
		chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
		chooser.setCurrentDirectory(FileSystems.getDefault().getPath(".").toFile());
		
		if( chooser.showOpenDialog(window) != JFileChooser.APPROVE_OPTION )
			return;
		
		// Find all instance files in it
		List<File> instanceFiles = new ArrayList<>();
		
		for(File currFile: chooser.getSelectedFile().listFiles()){
			// Instance files should have a ".txt" extension
			if( !currFile.getName().endsWith(".txt") )
				continue;
			
			// Solution files are .txt too, but have _sol at the end
			if( currFile.getName().endsWith("_sol.txt") )
				continue;
			
			// We found a good file, add it to the list
			instanceFiles.add(currFile);
		}
		
		// Import the files
		importInstanceFiles(instanceFiles);
	}
	
	/**
	 * Imports all the given instance files.
	 * @param files The files to import.
	 */
	private void importInstanceFiles(Collection<File> files){
		(new SwingWorker<Void,Void>() {
			@Override
			protected Void doInBackground() throws Exception {
				// Import everything
				int imported=0, total=files.size();
				List<String> failedImports = new ArrayList<>();
				
				// Show the progress notice
				importProgress.update(0, files.size(), "Preparing…");
				importProgress.setVisible(true);

				for(File currFile: files){
					// Update the progress bar
					importProgress.update(imported, total, String.format("%s (%d/%d)", currFile.getName(), imported, total));

					// Import the instance
					try(final FileInputStream fis = new FileInputStream(currFile)) {
						final BufferedInputStream bfis = new BufferedInputStream(fis);
						final Instance instance = Parser.parseInstanceFast(bfis);
						instance.setName(currFile.getName());
						
						// Add it to the database
						importProgress.update(imported, total, String.format("saving %s (%d/%d)", currFile.getName(), imported, total));

						if( !controller.getInstanceDao().create(instance) ){
							JOptionPane.showMessageDialog(window, "Failed to add instances to the database", "Database Error", JOptionPane.ERROR_MESSAGE);
						}
					} catch(IOException|SyntaxError ex){
						JOptionPane.showMessageDialog(window, "Error importing instance: " + ex.getMessage(), "Instance Import Error", JOptionPane.ERROR_MESSAGE);
					}
					
					++imported;
				}

				// Display feedback
				if( failedImports.isEmpty() ){
					JOptionPane.showMessageDialog(null, String.format("Successfully imported %d file(s)", files.size()));
				} else {
					String msg = String.format("Failed to import %d file(s) out of %d:\n%s", failedImports.size(), files.size(), null);

					for(String curr: failedImports)
						msg += String.format("\n - '%s'", curr);

					JOptionPane.showMessageDialog(null, msg);
				}
				
				importProgress.setVisible(false);
				onRefresh();
				return null;
			}
		}).execute();
	}

	@Override
	public void onRemoveAllInstances() {
		if( JOptionPane.showConfirmDialog(window, "Delete all instances?") != JOptionPane.YES_OPTION )
			return;
		
		(new SwingWorker<Void,Void>() {
			@Override
			protected Void doInBackground() throws Exception {
				SplashController splash = new SplashController();
				splash.update(0, "Removing all instances…");
				splash.open();
				
				Collection<Instance> instances = controller.getInstanceDao().findAll();
				
				int handled = 0;
				for(Instance curr: instances){
					splash.update(((float) handled) / ((float) instances.size()), curr.getName());
					controller.getInstanceDao().delete(curr);
					++handled;
				}
				
				onRefresh();
				
				splash.close();
				return null;
			}
		}).execute();
	}

	@Override
	public void onRemoveInstance(Instance instance) {
		if( JOptionPane.showConfirmDialog(window, "Delete this instance?") != JOptionPane.YES_OPTION )
			return;
		
		(new SwingWorker<Void,Void>() {
			@Override
			protected Void doInBackground() throws Exception {
				SplashController splash = new SplashController();
				splash.update(0, "Removing instance…");
				splash.open();
				
				if( !controller.getInstanceDao().delete(instance) )
					JOptionPane.showMessageDialog(window, "Failed to remove an instance from the database", "Database Error", JOptionPane.ERROR_MESSAGE);
				
				onRefresh();
				
				splash.close();
				return null;
			}
		}).execute();
	}

	@Override
	public void onRemoveSolution(Solution obj) {
		// TODO
		throw new UnsupportedOperationException("Not supported yet.");
	}
}
