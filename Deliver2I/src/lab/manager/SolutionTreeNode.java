package lab.manager;

import javax.swing.tree.DefaultMutableTreeNode;
import model.Solution;

/**
 * TreeNode implementation representing a solution.
 * @author jessy
 */
class SolutionTreeNode extends DefaultMutableTreeNode {
	final private Solution solution;
	
	public SolutionTreeNode(Solution solution){
		super(solution);
		this.solution = solution;
	}

	@Override
	public String toString() {
		return String.format("#%d (cout %d, %d vehicule/s)", solution.getId(), solution.getCost(), solution.getVehicules().size());
	}

	public Solution getSolution() {
		return solution;
	}
}
