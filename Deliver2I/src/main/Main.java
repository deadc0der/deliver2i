package main;

import algo.GenerativeSolver;
import algo.NoSolutionError;
import algo.SolverFactory;
import java.io.File;
import model.*;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import lab.AppController;
import parser.Parser;
import parser.SyntaxError;
import writer.SolutionWriter;

/**
 * Main class of the entire program.
 */
public class Main {

	public static void main(String[] args) {
		// Read the command line
		final CommandLine cmdLine = new CommandLine(args);

		if (cmdLine.hasOption("help")) {
			showHelp();
			return;
		}

		if (!cmdLine.hasFiles()) {
			// If there are no positional arguments, start the GUI.
			AppController.main(args);
		} else {
			// If there is a file name, it's an instance file so solve it
			for (String curr : cmdLine.getFiles()) {
				handleFile(curr);
			}
		}
	}

	/**
	 * Displays the help notice.
	 */
	private static void showHelp() {
		System.out.println("> pass a file or directory name to solve instances, or don't to open the GUI");
		System.out.println("> ");
		System.out.println("> options:");
		System.out.println(">  -dao reader  Read the instance files from the instance files in ../Data");
		System.out.println(">  -dao jpa     Use JPA to read instances from the database");
		System.out.println(">  -no-manager  Don't open the Instance Manager window");
		System.out.println(">  -no-bench    Don't open the Solver Tester window");
	}

	/**
	 * Reads an instance file at the given path.
	 *
	 * @param path The path to the instance file.
	 * @return The resulting Instance object.
	 * @throws FileNotFoundException if the file is not found.
	 * @throws IOException
	 */
	private static Instance readInstanceFile(String path) throws FileNotFoundException, IOException {
		// First try with the fast but fragile parser.
		try {
			FileInputStream inStream = new FileInputStream(path);
			return Parser.parseInstanceFast(inStream);
		} catch (SyntaxError err) {
			System.err.format("/!\\ Failed to read instance from '%s': %s\n", path, err.getMessage());
		}

		// If that didn't work, try the slow but robust parser.
		try {
			FileInputStream inStream = new FileInputStream(path);
			return Parser.parseInstance(inStream);
		} catch (SyntaxError err) {
			System.err.format("Syntax error in file '%s': %s\n", path, err.getMessage());
			return null;
		}
	}

	/**
	 * Solves the given instance.
	 *
	 * @param path Path to the instance file.
	 */
	private static void solveInstance(String path) {
		System.out.println("Solving instance: " + path);
		try {
			final File file = new File(path);

			// Read the instance file
			final Instance instance = readInstanceFile(path);

			if (instance == null) {
				return;
			}

			// Calculate a solution using MetaSolver
			final GenerativeSolver solver = SolverFactory.createGenerator(SolverFactory.Generator.Meta);
			final Solution solution = solver.solve(instance);
			System.out.println("Solution: cost=" + solution.getCost() + ", vehicles=" + solution.getVehicules().size());

			// Find out what file to put the solution in
			final String instanceName = file.getName();
			final String solutionName = instanceName.substring(0, instanceName.lastIndexOf(".")) + "_sol.txt";
			final String solutionsDir = file.getParent();
			final String solutionPath = solutionsDir + "/" + solutionName;

			// Write the solution to this file
			FileOutputStream outStream = new FileOutputStream(solutionPath);
			SolutionWriter writer = new SolutionWriter(outStream, solution);
			writer.write();
		} catch (FileNotFoundException err) {
			System.err.println("File not found: " + path);
		} catch (SolverFactory.GeneratorNotFound err) {
			System.err.println("Internal error: " + err.getMessage());
		} catch (NoSolutionError err) {
			System.err.println("No solution was found for instance: " + path);
		} catch (IOException err) {
			System.err.println("Failed to read file: " + path);
			System.err.println("I/O Error: " + err.getMessage());
		}
	}

	/**
	 * Reads an instance from a file or directory and solves it.
	 *
	 * @param path Path to the file. If the file is a directory, its contents
	 * are read recursively.
	 */
	private static void handleFile(String path) {
		// Make sure the file exists
		File file = new File(path);

		if (!file.exists()) {
			System.err.println("/!\\ File '" + path + "' does not exist");
			return;
		}

		if (file.isFile()) {
			// If it's a single file, solve it
			solveInstance(path);
		} else if (file.isDirectory()) {
			// If it's a directory, solve every file it contains
			System.out.println("Solving all instances in directory: " + path);

			for (File curr : file.listFiles()) {
				handleFile(curr.getPath());
			}
		}

	}
}
