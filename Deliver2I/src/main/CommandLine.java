package main;

import java.util.*;

/**
 * Command Line reading.
 */
public final class CommandLine {
	final private String [] args;
	final private Map<String, String> options;
	final private List<String> files;
	
	public CommandLine(String[] args) {
		this.args = args;
		this.options = new HashMap<>();
		this.files = new ArrayList<>();
		parseCommandLine();
	}
	
	/**
	 * Returns the list of files provided on the command line.
	 */
	public List<String> getFiles(){
		return files;
	}
	
	/**
	 * Returns whether any files are on the command line.
	 */
	public boolean hasFiles(){
		return !files.isEmpty();
	}
	
	/**
	 * Determines whether an option is provided.
	 * @param name The option name.
	 */
	public boolean hasOption(String name){
		return options.containsKey(name);
	}
	
	/**
	 * Returns the value of the given option.
	 * @param name The option name.
	 * @param defaultValue The value to return if the option isn't provided.
	 * @return The value, or the default value if the option is missing.
	 */
	public String getOption(String name, String defaultValue){
		return options.getOrDefault(name, defaultValue);
	}
	
	/**
	 * Returns the value of the given option.
	 * @param name The option name.
	 * @return The value, or null if the option is missing.
	 */
	public String getOption(String name){
		return getOption(name, null);
	}
	
	/**
	 * Returns the set of options.
	 */
	public Set<String> getOptionList(){
		return options.keySet();
	}
	
	/**
	 * Reads an option at the given index.
	 * @param i The index of the argument.
	 * @return The number of arguments the option's value consists of.
	 */
	private int parseOption(int i){
		// Look for a value.
		final String option = args[i].trim();
		final String optionName = option.startsWith("--") ? option.substring(2) : option.substring(1);
		
		// There can't be a value if there's nothing after that, or the
		// next argument is an option too.
		if( i < args.length - 1 && !args[i + 1].startsWith("-") ){
			options.put(optionName, args[i + 1].trim());
			return 1;
		} else {
			options.put(optionName, null);
			return 0;
		}
	}
	
	/**
	 * Reads the command line arguments.
	 */
	private void parseCommandLine(){
		for(int i=0; i < args.length; ++i){
			final String curr = args[i];
			
			if( curr.startsWith("-") ){ // Option
				i += parseOption(i);
			} else { // Positional (file)
				files.add(curr);
			}
		}
	}
}
