﻿Le checker peut être utilisé de 4 façons différentes.

1)	-p fichierInstance
	-p fichierInstance.txt
	
	Dans ce cas, le fichier instance et le fichier solution doivent être dans le même dossier et le fichier solution doit être nommé fichierInstance_sol.txt.
	Vous n'êtes pas obligés de donner l'extension de votre fichier, mais le fichier doit être de type txt.
	
2)	-p cheminInstance/fichierInstance cheminSolution/fichierSolution
	-p cheminInstance/fichierInstance.txt cheminSolution/fichierSolution.txt
	
	Dans ce cas, le fichier instance et le fichier solution peuvent être dans des répertoires différents et peuvent être nommés n'importe comment.
	Vous n'êtes pas obligés de donner l'extension de votre fichier, mais le fichier doit être de type txt.
	
3)	-d
	
	Dans ce cas, toutes les solutions dans le répertoire du checker sont testées. Tous les fichiers solutions doivent terminer par _sol.
	Les fichiers instances doivent être dans le même répertoire et nommés fichierInstance pour être associés à fichierInstance_sol.
	Vous n'êtes pas obligés de donner l'extension de vos fichiers, mais les fichiers doivent être de type txt.
	
4)	-d cheminRepertoire

	Dans ce cas, toutes les solutions dans le répertoire indiqué par cheminRepertoire sont testées. Ce répertoire ne doit pas forcement contenir le checker.
	Tous les fichiers solutions doivent terminer par _sol.
	Les fichiers instances doivent être dans le même répertoire et nommés fichierInstance pour être associés à fichierInstance_sol.
	Vous n'êtes pas obligés de donner l'extension de vos fichiers, mais les fichiers doivent être de type txt.